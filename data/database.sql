CREATE TABLE queue (
	`id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`site_url` VARCHAR(80) NOT NULL,
	`domain` VARCHAR(80) NOT NULL,
	`cpanel_url` VARCHAR(80) NOT NULL,
	`cpanel_user` VARCHAR(80) NOT NULL,
	`cpanel_psw` VARCHAR(80) NOT NULL,
	`plugin_404_301` TINYINT(2) NOT NULL DEFAULT 0,
	`package` VARCHAR(100) NOT NULL DEFAULT "",
	`date_created` DATETIME,
	`date_run` DATETIME,
	`process_time` INT(11) DEFAULT 0,
	`complete` TINYINT(2) DEFAULT 0
);

CREATE TABLE queue_creds (
	`q_id` INT(11) NOT NULL PRIMARY KEY,
	`dbname` VARCHAR(30) NOT NULL DEFAULT "",
	`db_user` VARCHAR(40) NOT NULL DEFAULT "",
	`db_psw` VARCHAR(64) NOT NULL DEFAULT "",
	`db_user_has_priv` TINYINT(2) NOT NULL DEFAULT 0,
	`ftp_user` VARCHAR(40) NOT NULL DEFAULT "",
	`ftp_psw` VARCHAR(64) NOT NULL DEFAULT "",
	FOREIGN KEY `fk_queue_id` (q_id) REFERENCES `queue`(id) 
);
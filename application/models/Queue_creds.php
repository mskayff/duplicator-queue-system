<?php


class Queue_creds extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}

	public function create($params){
		return $this->db->replace('queue_creds', $params);
	}

	public function insert_dbname($qid, $dbname){
		if (empty($dbname))
			throw new QueueCredentialsException ("Db name cannot be empty");		
		
		$this->db->where('q_id', $qid);

		return $this->db->update('queue_creds', array(				
				'dbname' => $dbname
			));
	}

	public function insert_db_user($qid, $db_user, $db_psw){

		$this->db->where('q_id', $qid);

		return $this->db->update('queue_creds', array(
				'db_user' => $db_user,
				'db_psw'  => $db_psw
			));
	}

	public function setUserPrivilege ($qid, $priv){
		return $this->db->where('q_id',$qid)
						->update('queue_creds', array(
								'db_user_has_priv' => $priv
							));
	}

	public function updatePrefix($qid, $prefix){
		if(empty($prefix))
			throw new QueueCredentialsException("Prefix cannot be empty(var)");

		return $this->db->where('q_id', $qid)
						->update('queue_creds', array(
							'prefix' => trim($prefix)
						));
	}

	public function insertFtpCredentials($qid, $user,$psw){

		if(empty($qid))
			throw new QueueCredentialsException ("Qid cannot be empty in FTP insert credentials");

		return $this->db->where('q_id', $qid)
						->update('queue_creds', array(
								'ftp_user' => $user,
								'ftp_psw' => $psw
							));
	}

	public function getById($qid){
		$query = $this->db->get_where('queue_creds', array('q_id' => $qid));

		return $query->row();
	}

}

class QueueCredentialsException extends Exception {}
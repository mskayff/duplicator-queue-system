<?php

function generate_random_password($options){
	
	$alpha = "abcdefghijklmnopqrstuvwxyz";
	$alpha_upper = strtoupper($alpha);
	$numeric = "0123456789";
	$special = ".-+=_,!@$#*%[]{}";
	$chars = "";
	 
	if (isset($options['length'])){
	    // if you want a form like above
	    if (isset($options['alpha']) && $options['alpha'] == 'on')
	        $chars .= $alpha;
	     
	    if (isset($options['alpha_upper']) && $options['alpha_upper'] == 'on')
	        $chars .= $alpha_upper;
	     
	    if (isset($options['numeric']) && $options['numeric'] == 'on')
	        $chars .= $numeric;
	     
	    if (isset($options['special']) && $options['special'] == 'on')
	        $chars .= $special;
	     
	    $length = $options['length'];
	}else{
	    // default [a-zA-Z0-9]{9}
	    $chars = $alpha . $alpha_upper . $numeric;
	    $length = 9;
	}
	 
	$len = strlen($chars);
	$pw = '';
	 
	for ($i=0;$i<$length;$i++)
	        $pw .= substr($chars, rand(0, $len-1), 1);
	 
	// the finished password
	$pw = str_shuffle($pw);

	return $pw;
}


function random_alpha ($str_length){

	$alpha = "abcdefghijklmnopqrstuvwxyz";

	$rstr = "";
	for ($i=0;$i<$str_length;$i++)
	        $rstr .= substr($alpha, rand(0, strlen($alpha)-1), 1);

	return str_shuffle($rstr);
}
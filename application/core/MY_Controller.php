<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 *
 * Top Controller class for Prospector
 *
 */
class MY_Controller extends CI_Controller{
	   /**
     *
     * A Json Response wrapper
     *
     */    
    public function jsonResponse($jstruct, $status=200){
        // ------------------------------------------------------------------------
        // Check if $jstruct is an object or array.
        // If it is we must json encode.
        // ------------------------------------------------------------------------
        if(is_array($jstruct) || is_object($jstruct))
            $out = json_encode($jstruct);
        else 
            $out = trim($jstruct);

        return $this->output->set_status_header($status)
                    ->set_content_type('application/json')
                    ->set_output($out);
    }

}
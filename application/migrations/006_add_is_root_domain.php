<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Is_Root_Domain extends CI_Migration {
	public function up(){
		$this->dbforge->add_column('queue', [
			'is_root_domain' => [
				'type'       => 'TINYINT',
				'constraint' => 2,
				'null' => false,
				'default' => 0,
				'after' => 'package'
			]
		]);
	}
	public function down(){
		$this->dbforge->drop_column('queue', 'is_root_domain');
	}
}
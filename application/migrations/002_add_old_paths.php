<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_old_paths extends CI_Migration {
	

	public function up(){
		$columns = array(
				'old_url' => array(
						'type' => 'VARCHAR',
						'constraint' => '150',
						'null' => false,
						'default' => '',
						'after' => 'domain'
					),
				'old_doc_root' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => false,
						'default' => '',
						'after' => 'old_url'
					)
			);

		$this->dbforge->add_column('queue', $columns);
	}

	public function down(){
		$this->dbforge->drop_column('queue', 'old_url');
		$this->dbforge->drop_column('queue', 'old_doc_root');
	}

}
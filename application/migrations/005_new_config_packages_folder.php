<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_new_config_packages_folder extends CI_Migration{
	public function up(){
		$this->db->insert('config',[
			'option_name' => 'packages_folder',
			'option_value' => ""
		]);
	}
	public function down(){
		$this->db->where('option_name', 'packages_folder')->delete('config');
	}
}
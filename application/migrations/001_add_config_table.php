<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Config_Table extends CI_Migration {
	public function up(){

		$this->dbforge->add_field('id');
		$this->dbforge->add_field(array(				
				'option_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45'
					),
				'option_value' => array(
						'type' => 'VARCHAR',
						'constraint' => '200'
					)
			));
		$this->dbforge->create_table('config',true);

		$this->db->insert('config', array(
				'option_name' => 'old_url_root',
				'option_value' => ''
			));
		$this->db->insert('config', array(
				'option_name' => 'old_doc_root',
				'option_value' => ''
			));
	}

	public function down(){
		$this->dbforge->drop_table('config');
	}
}
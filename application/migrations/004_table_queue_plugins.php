<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_table_queue_plugins extends CI_Migration {
	public function up(){
		$this->dbforge->add_field([
			'qid' => [
					'type' => 'INT',
                	'constraint' => 11,                
				],
			'plugin_404_301' => [
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0
				],
			'remove_widget_titles' => [
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0
				],
			'allow_php_post_pages' => [
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0
				],
			'flexi_pages_widget'   => [
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0
				],
			'new_random_post' =>[
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0
				]
		]);

		$this->dbforge->add_field('CONSTRAINT FOREIGN KEY (qid) REFERENCES queue(id) ON DELETE CASCADE ON UPDATE CASCADE');
		$this->dbforge->create_table('queue_plugins', true);

		// ---------------------------------------------------
		// Remove the 404 to 301 plugin
		// ---------------------------------------------------
		$this->dbforge->drop_column('queue', 'plugin_404_301');
	}
	public function down(){
		$this->dbforge->drop_table('queue_plugins', TRUE);

		$this->dbforge->add_column('queue', [
			'plugin_404_301' => [
					'type' => 'TINYINT',
					'constraint' => 2,
					'default'    => 0,
					'after'      => 'cpanel_psw'
				],
		]);
	}
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Prefix_To_Creds extends CI_Migration{
	
	public function up (){
		$this->dbforge->add_column('queue_creds', array(
				'prefix' => array(
						'type' => 'VARCHAR',
						'constraint' => 20,
						'default' => '',
						'after' => 'q_id'
					)
			));
	}

	public function down(){
		$this->dbforge->drop_column('queue_creds', 'prefix');
	}

}
<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title></title>
	<base href="<?php echo config_item('base_url');?>">
	<link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- JQuery File Uploads CSS -->
	<link rel="stylesheet" href="assets/jquery/jquery-file-upload/css/jquery.fileupload.css">

	<!-- JS -->
	<script type="text/javascript" src="assets/jquery/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
	
<div class="container-fluid">
	<?php if(isset($show_back) && $show_back == true) : ?>
		<div class="top-nav-control pull-left">
			<a href="duplicator/index" class="btn btn-default">&lt;&lt; Back</a>
		</div>
	<?php endif; ?>
	<div class="top-nav-control pull-right">
		<a href="<?=base_url('new');?>" class="btn btn-success">Add new</a>
		<a href="<?=base_url('import-csv');?>" class="btn btn-primary">Import CSV</a>
		<a href="<?=base_url('config');?>" class="btn btn-default">Config</a>
		<!--<a class="btn btn-primary">History</a>-->
	</div>
</div>
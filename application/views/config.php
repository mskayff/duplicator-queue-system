<?php
$this->load->view('template/header', array('show_back' => true));
?>

<div class="container-fluid">

	<div class="page-title">
		<h3>Configuration</h3>
	</div>

<?php if(!empty($error)) : ?>

	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<span><strong>Following errors have occured:</strong></span><br>
		<?php echo implode("<br>", $error); ?>
	</div>

<?php endif;?>

	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old Url Root</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="old_url_root" placeholder="Old url root" value="<?=set_value('old_url_root', $old_url_root);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old Document Root</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="old_doc_root" placeholder="Old document root" value="<?=set_value('old_doc_root', $old_doc_root);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Packages folder</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="packages_folder" placeholder="Packages folder path" value="<?=set_value('packages_folder', $packages_folder);?>">
				<span class="help-block">Packages path for the csv importing tool</span>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-5 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Update config</button>
			</div>
		</div>

	</form>

</div>

<?php
$this->load->view('template/footer');
?>
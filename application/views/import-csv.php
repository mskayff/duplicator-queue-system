<?php
$this->load->view('template/header');
?>

<div class="container-fluid">

<?php	
	if ($packages_folder_warning) : 
?>
	<div class="alert alert-danger">		
		<strong>Warning!</strong> You do not have set the "Packages Folder" in the config. This is required to be able to parse the csv file succesfully!
		Go <a href="<?=base_url('config')?>">here</a> to update this field
	</div>
<?php 
	endif;
?>

	<div class="row">
		<div class="col-md-6">
			<fieldset class="form">
				<legend>Import CSV file</legend>
				<div class="form-group">
					<span class="btn btn-success fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span>Select a csv file</span>
						<input type="file" id='csv-upload' name="csv">
					</span>	
				</div>										
				<!-- The global progress bar -->
				<div class="form-group">
			    	<div id="progress" class="progress" style="display:none;">
			        	<div class="progress-bar progress-bar-success"></div>
			    	</div>	
		    	</div>
			</fieldset>
		</div>
	</div>

	<div class="row" id="results-header" style="display:none;">
		<div class="col-xs-6 col-md-4 col-lg-3">
			<div class="panel panel-default">
				<div class="panel-heading">Result:</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 text-center">
							<h4>Success:</h4>
							<span class="counter counter-success" id="success-count">5</span>
						</div>
						<div class="col-xs-6 text-center">
							<h4>Failed:</h4>
							<span class="counter counter-fail" id="failed-count">1</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6" id="result-container">

		</div>
	</div>

</div>
<script>
	$(function() {
		'use strict';
		$('#csv-upload').fileupload({
			url : 'duplicator/csv-upload',
			dataType : 'json',
			done : function(e, data){
				// ---------------------------------------------------
				// Get the response
				// ---------------------------------------------------
				var response = data.result;
				if(response.status == "ok"){
					$('#success-count').html(response.success_count);
					$('#failed-count').html(response.failed_count);
					$('#results-header').show();
					// ---------------------------------------------------
					// Loop through every element in the response
					// ---------------------------------------------------
					response.result.forEach(function(item){

						var $lineResultBox = $("<div>");
						// ---------------------------------------------------
						// Check the status of each individual line
						// ---------------------------------------------------
						if(item.status == "ok"){
							$lineResultBox.addClass("alert alert-success");
							$lineResultBox.html("Line " + item.line + " success! " + "Site: " + item.site_url);
						}	
						else{
							$lineResultBox.addClass("alert alert-danger");
							$lineResultBox.html(item.error);
						}	
						$('#result-container').append($lineResultBox);
					});
				}
				else{
					// ---------------------------------------------------
					// We have an error in the response
					// ---------------------------------------------------
					var $errorMsg = $("<div>").addClass("alert alert-danger")
										.html(response.msg);

					$('#result-container').append($errorMsg);
				}
			},
			progress: function (e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress').show();
	            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
	        },
	        start : function(e){
	        	// ---------------------------------------------------
	        	// Clean the result container
	        	// ---------------------------------------------------
	        	$('#result-container').empty();
	        },
		});

	});
</script>
<?php
$this->load->view('template/footer');
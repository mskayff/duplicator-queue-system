<?php
$this->load->view('template/header');
?>
<div class="container-fluid">

	<div class="page-title">
		<h3>Edit queued package</h3>
	</div>


<?php if(!empty($error)) : ?>

	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<span><strong>Follong errors have occured:</strong></span><br>
		<?php echo implode("<br>", $error); ?>
	</div>

<?php endif; ?>

	
	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" id="old_url_root" name="old_url_root" required value="<?php echo set_value("old_url_root", $queue->old_url);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old doc root</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" id="old_doc_root" name="old_doc_root" required value="<?php echo set_value("old_doc_root", $queue->old_doc_root);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Site Url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="site_url" placeholder="" required value="<?php echo set_value('site_url', $queue->site_url); ?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_url" placeholder="" required value="<?php echo set_value('cpanel_url', $queue->cpanel_url); ?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel user</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_user" placeholder="" required value="<?php echo set_value('cpanel_user', $queue->cpanel_user); ?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel password</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_password" placeholder="" required value="<?php echo set_value('cpanel_password', $queue->cpanel_psw);?>">
			</div>
		</div>

		<div class="form-group">
			<div class="checkbox col-sm-offset-2">
				<label for="" class="control-label">
					<input type="checkbox" name="is_root_domain" value="1" <?=set_value('is_root_domain', $queue->is_root_domain)? "CHECKED" : "";?>>
					Is Root Domain
				</label>				
			</div>			
		</div>

		<div class="row" >
			<h4 class="col-sm-5 col-sm-offset-1 panel-subtitle" >Plugins:</h4>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Add plugin 404 to 301</label>
			<div class="col-sm-5 checkbox">
				<label>
			    	<input type="checkbox" name="plugin_404_301" <?php echo set_value('plugin_404_301', $queue->plugin_404_301)? "checked" : "";?>>
			    </label>				
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Remove Widget Titles</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="remove_widget_titles" <?php echo set_value('remove_widget_titles', $queue->remove_widget_titles)? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Allow PHP in Post and Pages</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="allow_php_post_pages" <?php echo set_value('allow_php_post_pages', $queue->allow_php_post_pages)? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Flexi Pages Widget</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="flexi_pages_widget" <?php echo set_value('flexi_pages_widget', $queue->flexi_pages_widget)? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Random Post</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="new_random_post" <?php echo set_value('new_random_post', $queue->new_random_post)? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Select package file</label>
			<div class="col-sm-5 col-lg-4">
				<input type="file" name="package_file">
			</div>
		</div>

		<!-- Specific collected credentials. Can be edited though! -->
		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Prefix</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="prefix" value="<?php echo set_value('prefix', $queue->prefix);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Db name</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="db_name" value="<?php echo set_value('db_name', $queue->dbname);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Db user</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="db_user" value="<?php echo set_value('db_user', $queue->db_user);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Db password</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="db_psw" value="<?php echo set_value('db_psw', $queue->db_psw);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Ftp username</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="ftp_user" value="<?php echo set_value('ftp_user', $queue->ftp_user);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Ftp password</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="ftp_psw" value="<?php echo set_value('ftp_psw', $queue->ftp_psw);?>">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-5 col-lg-4 col-sm-offset-2">
				<button class="btn btn-primary">Update</button>
			</div>
		</div>

	</form>


</div>

<?php
$this->load->view('template/footer');
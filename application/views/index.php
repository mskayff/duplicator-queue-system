<?php
$this->load->view('template/header');
?>
<div class="container-fluid">

<?php if (!empty($this->session->flashdata('success'))) : ?>

	<div class="alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo $this->session->flashdata('success'); ?>
	</div>

<?php endif; ?>


<?php 
	// Show any possible error messages
	if (!empty($this->session->flashdata('error'))) : 
?>

	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo $this->session->flashdata('error'); ?>
	</div>

<?php endif; ?>


	<div class="row">
		<div class="col-sm-6">
			<div class="panel-controls">
				<h4 class="panel-title">Queue</h4>
				<button id="btn-process-queue" class="btn btn-warning pull-right" <?php echo empty($queue)? "DISABLED" : "";?>>Process queue</button>
			</div>
			<div class="panel-ms queue-panel">
		<?php 
			if (!empty($queue)) :
				foreach ($queue as $qItem) : 
		 ?>
				<div id="queue-item-<?=$qItem->id;?>" class="queue-item-box queue-item-idle">
					<div class="row">
						<div class="col-xs-4">
							<label>Domain: </label><br>
							<span><?php echo $qItem->site_url;?></span>
						</div>
						<div class="col-xs-3">
							<label>Created: </label><br>
							<span><?php echo $qItem->date_created;?></span>
						</div>
						<div class="col-xs-2"></div>
						<div class="col-xs-3" style="padding-top:5px;">													
										
							<div class="pull-right">
								<a href="duplicator/edit/<?=$qItem->id?>" class="btn btn-default queue-item-edit-icon">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							
							
								<a class="btn btn-default btn-remove-package queue-item-edit-icon" data-qid="<?php echo $qItem->id;?>">
									<img src="assets/images/red-cross-1.png">
								</a>
							</div>						
							
						</div>						
					</div>
				</div>

		<?php 
				endforeach;
			else : 
		?>
				<div>The queue is empty now</div>

		<?php 
			endif;
		?>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel-controls">
				<h4 class="panel-title">Terminal <span id="terminal-loader" style="display:none;"><img src="assets/images/slider-preloader.gif"></span></h4>
				<div class="pull-right">
					<button id='btn-reset-terminal' class="btn btn-default">Reset</button>
					<button id="btn-stop-process" class="btn btn-default">Stop</button>
				</div>
			</div>
			<div id="terminal-panel" class="panel-ms terminal-panel"></div>
		</div>
	</div>		
</div>


<!-- Modal Remove Package -->
<div class="modal fade" id="delete-package-modal" tabindex="-1" role="dialog" aria-labelledby="deletePackageModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove this package from the queue?</h4>
      </div>
      <div class="modal-body">
       		This will permantently remove all data related to this package
      </div>
      <div class="modal-footer">
      	<button type="button" id="btn-delete-package" data-qid="" class="btn btn-danger">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>

<!-- Modal Process Queue -->
<div class="modal fade" id="process-queue-modal" tabindex="-1" role="dialog" aria-labelledby="deletePackageModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Process queue</h4>
      </div>
      <div class="modal-body">
       		This will process <span id="confirm-package-count"></span> items in the queue. Proceed?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn-confirm-process-queue" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>


<?php
$this->load->view('template/footer');
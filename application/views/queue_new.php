<?php
$this->load->view('template/header', array('show_back' => true));
?>

<div class="container-fluid">

	<div class="page-title">
		<h3>Add new package to queue</h3>
	</div>


<?php if(!empty($error)) : ?>

	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<span><strong>Following errors have occured:</strong></span><br>
		<?php echo implode("<br>", $error); ?>
	</div>

<?php endif; ?>

	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Folder</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" id="folder_syncro" name="folder_syncro" value="<?php echo set_value('folder_syncro');?>">
			</div>
			<div class="col-sm-1 checkbox">
				<label>
					<input type="checkbox" id="sync_paths" CHECKED> Sync
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" id="old_url_root" name="old_url_root" required readonly value="<?php echo set_value("old_url_root", $old_url_root);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Old doc root</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" id="old_doc_root" name="old_doc_root" required readonly value="<?php echo set_value("old_doc_root", $old_doc_root);?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Site Url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="site_url" placeholder="" required value="<?php echo set_value('site_url');?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel url</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_url" placeholder="" required value="<?php echo set_value('cpanel_url');?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel user</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_user" placeholder="" required value="<?php echo set_value('cpanel_user');?>">
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">cPanel password</label>
			<div class="col-sm-5 col-lg-4">
				<input type="text" class="form-control" name="cpanel_password" placeholder="" required value="<?php echo set_value('cpanel_password');?>">
			</div>
		</div>

		<div class="form-group">
			<div class="checkbox col-sm-offset-2">
				<label for="" class="control-label">
					<input type="checkbox" name="is_root_domain">
					Is Root Domain
				</label>				
			</div>			
		</div>

		<div class="row" >
			<h4 class="col-sm-5 col-sm-offset-1 panel-subtitle" >Plugins:</h4>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Add plugin 404 to 301</label>
			<div class="col-sm-5 checkbox">
				<label>
			    	<input type="checkbox" name="plugin_404_301" <?php echo set_value('plugin_404_301', 'checked')? "checked" : "";?>>
			    </label>				
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Remove Widget Titles</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="remove_widget_titles" <?php echo set_value('remove_widget_titles', '')? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Allow PHP in Post and Pages</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="allow_php_post_pages" <?php echo set_value('allow_php_post_pages', '')? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Flexi Pages Widget</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="flexi_pages_widget" <?php echo set_value('flexi_pages_widget', 'checked')? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Random Post</label>
			<div class="col-sm-5 checkbox">
				<label>
					<input type="checkbox" name="new_random_post" <?php echo set_value('new_random_post', 'checked')? "checked" : "";?> >
				</label>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-2 control-label">Select package file</label>
			<div class="col-sm-5 col-lg-4">
				<input type="file" name="package_file" required>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-5 col-sm-offset-2">
				<button class="btn btn-primary">Add new</button>
			</div>
		</div>

	</form>
</div>

<script>	

	$(document).ready(function(e){
		// The directory separator
		var dirSeparator = "\<?=DIRECTORY_SEPARATOR;?>";
		var old_url_root = $("#old_url_root").val();
		var old_doc_root = $("#old_doc_root").val();

		$("#folder_syncro").keyup(function(e){

			if($("#sync_paths").is(":checked")){
				var folder = $(this).val();

				if(old_url_root.substr(-1) != "/"){
					old_url_root += "/";
				}

				if(old_doc_root.substr(-1) != dirSeparator){
					old_doc_root += dirSeparator;
				}

				$("#old_url_root").val(old_url_root + folder);
				$("#old_doc_root").val(old_doc_root + folder);
			}
		});	

		/**
		 * Swap the readonly state of "old url root" and "old doc root"
		 */
		$("#sync_paths").change(function(e){

			if($(this).is(":checked")){
				$("#old_url_root").prop('readonly', true);
				$("#old_doc_root").prop('readonly', true);
			}
			else{
				$("#old_url_root").prop('readonly', false);
				$("#old_doc_root").prop('readonly', false);
			}

		});
	});

</script>

<?php
$this->load->view('template/footer');
?>


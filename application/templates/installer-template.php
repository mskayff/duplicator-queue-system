<?php
/**
 * Installer.
 * Communicates with JSON to provide output feedback
 */

// ---------------------------------------------------
// Define the Home dir
// ---------------------------------------------------
define ('HOME_DIR', dirname(__FILE__));
// ---------------------------------------------------
// Define the WP plugins dir
// ---------------------------------------------------
define ('WP_PLUGIN_DIR', HOME_DIR . '/wp-content/plugins/');
// ---------------------------------------------------
// WP-Contents upload folder
// ---------------------------------------------------
define ('WP_UPLOADS_DIR', HOME_DIR . '/wp-content/uploads/');
// ---------------------------------------------------
// Store some globals here
// ---------------------------------------------------
$GLOBALS = array();

$site_url = '{{site_url}}';
$domain_name = '{{domain}}';

// ----------------
// Old setup data
// ----------------
$old_url_root = '{{old_url_root}}';
$old_doc_root = '{{old_doc_root}}';

// Database info
$db_host = '{{db_host}}';
$db_name = '{{db_name}}';
$db_user = '{{db_user}}';
$db_psw = '{{db_password}}';

// Package file name
$package_file = '{{package_name}}';

// ---------------------------------------------------
// Plugin flags to keep/remove plugins
// ---------------------------------------------------
$plugin_404_301 				= {{plugin_404_301}};
$plugin_remove_widget_titles 	= {{remove_widget_titles}};
$plugin_allow_php_post_pages    = {{allow_php_post_pages}};
$plugin_flexi_pages_widget	    = {{flexi_pages_widget}};
$plugin_new_random_post         = {{new_random_post}};

// ---------------------------------------------------
// The duplicator SQL file
// ---------------------------------------------------
$restore_sql_file = 'database.sql';

/**
 * A response array
 */
$response = array(
	'status' => 0,
	'extraction' => false,
	'db_clone'   => false,
	'wp_config'  => false,
	'file_clean'   => false,
	'plugin_removal' => false
);


// Check ZipArchive class exists
if(!class_exists('ZipArchive')){
	die(json_encode(array(
			'status' => 500,
			'error' => 'Missing ZipArchive module',
			'error_msg' => 'Error. Execution STOPPED! Trying to extract without ZipArchive module installed.'
		)));
}

try{
	// ---------------------------
	// Open a MySQLi connection
	// ---------------------------
	$db = new MySQLi($db_host,$db_user,$db_psw,$db_name);

	if ($db->connect_error) {
    	throw new Exception('Could not connect to database');
	}

	// ---------------------------------------------
	// Set the DB collation and charset
	// ---------------------------------------------
    if (function_exists('mysqli_set_charset')) {
        $db->set_charset('utf8');
    } else {
        $sql = " SET NAMES 'utf8'";
        if (!empty($collate))
            $sql .= " COLLATE 'utf8_general_ci'";
         $db->query($sql);
    }
    

	// ----------------------
	// 1 - Unzip the package
	// ----------------------
	$zip = new ZipArchive();

	if($zip->open(HOME_DIR . '/' . $package_file) === TRUE){
		$zip->extractTo(HOME_DIR);
		$response['extraction'] = true;
	}
	else{		
		throw new Exception ('Zip Exctraction failed.');
	}

	// -----------------------
	// Clone the database
	// -----------------------

	// ---------------------------------------------------
	// SET FOREIGN KEY CHECKS to 0
	// ---------------------------------------------------
	$db->query("SET FOREIGN_KEY_CHECKS = 0");

	//$db_restore_cmd = "mysql -h localhost -u {$db_user} -p{$db_psw} {$db_name} < $restore_sql_file";	
	//exec($db_restore_cmd);
	$sql_file = file_get_contents('database.sql', true);
	if ($sql_file == false || strlen($sql_file) < 10) {
		$sql_file = file_get_contents('installer-data.sql', true);
		if ($sql_file == false || strlen($sql_file) < 10) {
			throw new Exception ('SQL file could not be loaded in the server.');
		}
	}

	$sql_result_file_data = explode(";\n", $sql_file);
	$sql_result_file_length = count($sql_result_file_data);

	$sql_file = null;

	// Disable autocommit	
	@$db->autocommit(false);

	$counter = 0;
	while ($counter < $sql_result_file_length) {
		$query_strlen = strlen(trim($sql_result_file_data[$counter]));

		$qres = @$db->query($sql_result_file_data[$counter]);

		if(!empty($db->error)){
			throw new Exception('DB Restore Error: at ' . $sql_result_file_data[$counter]);
		}

		$counter++;
	}
	@$db->commit();
	@$db->autocommit(true);

	$response['db_clone'] = true;

	// -----------------
	// End db clone
	// -----------------
	


	// ---------------------------
	// Configure the WP-Config
	// ---------------------------
	$wpconfig   = @file_get_contents('wp-config.php', true);

	$patterns = array(
			"/'DB_NAME',\s*'.*?'/",
			"/'DB_USER',\s*'.*?'/",
			"/'DB_PASSWORD',\s*'.*?'/",
			"/'DB_HOST',\s*'.*?'/");

	$replace = array(
			"'DB_NAME', "	  . '\'' . $db_name				. '\'',
			"'DB_USER', "	  . '\'' . $db_user				. '\'',
			"'DB_PASSWORD', " . '\'' . preg_replacement_quote($db_psw) . '\'',
			"'DB_HOST', "	  . '\'' . $db_host				. '\'');

	//SSL CHECKS
	if ($_POST['ssl_admin']) {
		if (! strstr($wpconfig, 'FORCE_SSL_ADMIN')) {
			$wpconfig = $wpconfig . PHP_EOL . "define('FORCE_SSL_ADMIN', true);";
		}
	} else {
		array_push($patterns, "/'FORCE_SSL_ADMIN',\s*true/");
		array_push($replace,  "'FORCE_SSL_ADMIN', false");
	}

	if ($_POST['ssl_login']) {
		if (! strstr($wpconfig, 'FORCE_SSL_LOGIN')) {
			$wpconfig = $wpconfig . PHP_EOL . "define('FORCE_SSL_LOGIN', true);";
		}
	} else {
		array_push($patterns, "/'FORCE_SSL_LOGIN',\s*true/");
		array_push($replace, "'FORCE_SSL_LOGIN', false");
	}

	//CACHE CHECKS
	if ($_POST['cache_wp']) {
		if (! strstr($wpconfig, 'WP_CACHE')) {
			$wpconfig = $wpconfig . PHP_EOL . "define('WP_CACHE', true);";
		}
	} else {
		array_push($patterns, "/'WP_CACHE',\s*true/");
		array_push($replace,  "'WP_CACHE', false");
	}
	if (! $_POST['cache_path']) {
		array_push($patterns, "/'WPCACHEHOME',\s*'.*?'/");
		array_push($replace,  "'WPCACHEHOME', ''");
	}

	if (! is_writable(HOME_DIR . "/wp-config.php") ) 
	{
		if (file_exists(HOME_DIR . "/wp-config.php")) 
		{
			if(!chmod(HOME_DIR . "/wp-config.php", 0644)){
				throw new Exception ("Could not change permissions to wp-config.php");
			}				
				
		} else {
			throw new Exception ("File wp-config was not found.");
		}
	}
	

	$wpconfig = preg_replace($patterns, $replace, $wpconfig);
	file_put_contents('wp-config.php', $wpconfig);
	$wpconfig = null;
	$response['wp_config'] = true;	

	// ----------------------------------------------------------------------------------------
	// Prepend the http:// to the site_url if no http protocol specified
	// ----------------------------------------------------------------------------------------
	if(!preg_match('/^(http)?s?\:/', $site_url)){
		// ----------------------------------------------------------------------------------------
		// First remove any double forward slash if present
		// ----------------------------------------------------------------------------------------
		$site_url = 'http://' . str_replace('//', '', $site_url);
	}
	
	// ---------------------------------
	// Run some option modifications
	// ---------------------------------
	$db->query("UPDATE wp_options
					SET option_value='{$site_url}'
					WHERE option_name='siteurl'");
	$db->query("UPDATE wp_options
					SET option_value='{$site_url}'
					WHERE option_name='home'");


	// -----------------------------------------------------------------
	// Process some database string modifications for old paths
	// -----------------------------------------------------------------
	$url_old_json = str_replace('"', "", json_encode($old_url_root));
	$url_new_json = str_replace('"', "", json_encode($site_url));
	$path_old_json = str_replace('"', "", json_encode($old_doc_root));
	$path_new_json = str_replace('"', "", json_encode(dirname(__FILE__)));

	// Make REPLACEE_LIST an array
	$GLOBALS['REPLACE_LIST'] = array();

	array_push($GLOBALS['REPLACE_LIST'], 
		array('search' => $old_url_root,			 'replace' => $site_url), 
		array('search' => $old_doc_root,			 'replace' => dirname(__FILE__)), 
		array('search' => $url_old_json,				 'replace' => $url_new_json), 
		array('search' => $path_old_json,				 'replace' => $path_new_json), 	
		array('search' => urlencode($old_doc_root), 'replace' => urlencode(dirname(__FILE__))), 
		array('search' => urlencode($old_url_root),  'replace' => urlencode($old_url_root)),
		array('search' => rtrim(unset_safe_path($old_doc_root), '\\'), 'replace' => rtrim(dirname(__FILE__), '/'))
	);

	array_walk_recursive($GLOBALS['REPLACE_LIST'], _dupx_array_rtrim);

	$all_tables = get_database_tables($db);

	@$db->autocommit(false);
	$report = DUPX_UpdateEngine::load($db, $GLOBALS['REPLACE_LIST'], $all_tables, true);
	@$db->commit();
	@$db->autocommit(true);

	// ---------------------------------------------------
	// Restore the FOREIGN_KEY_CHECKS to 1
	// ---------------------------------------------------
	$db->query("SET FOREIGN_KEY_CHECKS = 1");

	// ---------------------------------------------------
	// ---------------------------------------------------
	// Plugins Management
	// ---------------------------------------------------
	// ---------------------------------------------------

	// --------------------------
	// Manage Plugin 404-301
	// --------------------------
	if($plugin_404_301){
		// ---------------------------------------------------
		// Keep the plugin and run some updates
		// ---------------------------------------------------
		$res = $db->query ("SELECT * FROM wp_options WHERE option_name='link'");
		if($res->num_rows > 0){
			$db->query("UPDATE wp_options
					SET option_value='{$site_url}'
					WHERE option_name='link'");
			$db->query("UPDATE wp_options
					SET option_value='301'
					WHERE option_name='type'");
		}
		else{
			$db->query("INSERT INTO wp_options
						SET option_name='link',
						option_value='{$site_url}'");
			$db->query("INSERT INTO wp_options
						SET option_name='type',
						option_value='301'");
		}		
	}
	// ---------------------------------------------------
	// Remove the 404_301 plugin
	// ---------------------------------------------------
	elseif (is_dir(WP_PLUGIN_DIR . '404-to-301')){
		rrmdir (WP_PLUGIN_DIR . '404-to-301');
		// Delete wp_option entries for plugin
		$db->query("DELETE FROM wp_options WHERE option_name IN ('link','type') LIMIT 2");	
	}
	// -----------------------------------------------------------------------------------
	// Keep/remove plugins.
	// If plugin flag is flase it means we want the plugin removed
	// -----------------------------------------------------------------------------------
	if(!$plugin_remove_widget_titles && is_dir(WP_PLUGIN_DIR . 'remove-widget-titles')){
		rrmdir(WP_PLUGIN_DIR . 'remove-widget-titles');
	}
	if(!$plugin_allow_php_post_pages && is_dir(WP_PLUGIN_DIR . 'allow-php-in-posts-and-pages')){
		rrmdir(WP_PLUGIN_DIR . 'allow-php-in-posts-and-pages');
	}
	if(!$plugin_flexi_pages_widget && is_dir(WP_PLUGIN_DIR . 'flexi-pages-widget')){
		rrmdir(WP_PLUGIN_DIR . 'flexi-pages-widget');
	}
	if(!$plugin_new_random_post && is_dir(WP_PLUGIN_DIR . 'newrandmpost')){
		rrmdir(WP_PLUGIN_DIR . 'newrandmpost');
	}

	// ---------------------------------------------------------------------------
	// Force remove the following plugins if present
	// ---------------------------------------------------------------------------
	$plugins_direct_remove = array(
		'title-and-nofollow-for-links',
		'duplicator',
		'site-settings-boss',
		'advanced-database-cleaner',
		'copyscape-premium',
		'wp-ultimate-csv-importer'
	);

	foreach($plugins_direct_remove as $pr){
		if(is_dir (WP_PLUGIN_DIR . $pr)){
			rrmdir(WP_PLUGIN_DIR . $pr);
			// ---------------------------------------------------
			// Plugin specific removal actions
			// ---------------------------------------------------
			if($pr == 'wp-ultimate-csv-importer'){
				$db->query('DROP TABLE IF EXISTS smackcsv_line_log');
				$db->query('DROP TABLE IF EXISTS smackcsv_pie_log');
				// ---------------------------------------------------
				// Remove ultimage-importer folder from uploads
				// ---------------------------------------------------
				rrmdir(WP_UPLOADS_DIR . 'ultimate_importer');
			}
		}
	}	
	$response['plugin_removal'] = true;

	// ---------------------------------------------------
	// End plugin management
	// ---------------------------------------------------


	// ---------------------------
	// Remove some unused files
	// ---------------------------
	@unlink (HOME_DIR . '/installer-backup.php');
	@unlink (HOME_DIR . '/installer-data.sql');
	@unlink (HOME_DIR . '/installer-log.txt');
	@unlink (HOME_DIR . '/.htaccess.orig');
	@unlink (HOME_DIR . '/' . $package_file);

	$response['file_clean'] = true;

	// Final succesfull status
	$response['status'] = '200';
}
catch(Exception $e){
	// ---------------------------------------------------
	// Fall back to the FOREIGN_KEY_CHECKS = 1
	// ---------------------------------------------------
	$db->query("SET FOREIGN_KEY_CHECKS = 1");
	
	$response['status'] = 500;
	$response['error'] = 'Exception occurred!';
	$response['error_msg'] = $e->getMessage();
	die (json_encode($response));
}

print json_encode($response);



function preg_replacement_quote($str) {
	return preg_replace('/(\$|\\\\)(?=\d)/', '\\\\\1', $str);
}

function rrmdir($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($dir."/".$object) == "dir") 
           rrmdir($dir."/".$object); 
        else unlink   ($dir."/".$object);
      }
    }
    reset($objects);
    rmdir($dir);
  }
}

function get_database_tables($dbh) {
    $query = @$dbh->query('SHOW TABLES');
    if ($query) {        
        while ($table = @$query->fetch_array()) {
            $all_tables[] = $table[0];
        }
        if (isset($all_tables) && is_array($all_tables)) {
            return $all_tables;
        }
    }
    return array();
}



/** * *****************************************************
 * CLASS::DUPX_UpdateEngine
 * Walks every table in db that then walks every row and column replacing searches with replaces
 * large tables are split into 50k row blocks to save on memory. */
class DUPX_UpdateEngine 
{	
	
	/**
	 * Returns only the text type columns of a table ignoring all numeric types
	 */
	public static function getTextColumns($db, $table) 
	{
		$type_where  = "type NOT LIKE 'tinyint%' AND ";
		$type_where .= "type NOT LIKE 'smallint%' AND ";
		$type_where .= "type NOT LIKE 'mediumint%' AND ";
		$type_where .= "type NOT LIKE 'int%' AND ";
		$type_where .= "type NOT LIKE 'bigint%' AND ";
		$type_where .= "type NOT LIKE 'float%' AND ";
		$type_where .= "type NOT LIKE 'double%' AND ";
		$type_where .= "type NOT LIKE 'decimal%' AND ";
		$type_where .= "type NOT LIKE 'numeric%' AND ";
		$type_where .= "type NOT LIKE 'date%' AND ";
		$type_where .= "type NOT LIKE 'time%' AND ";
		$type_where .= "type NOT LIKE 'year%' ";

		$result = $db->query("SHOW COLUMNS FROM `{$table}` WHERE {$type_where}");
		if (!$result) { 
			return null;
		} 
		$fields = array(); 
		if ($result->num_rows > 0) { 
			while ($row = $result->fetch_assoc()) { 
				$fields[] = $row['Field']; 
			} 
		} 
		
		//Return Primary which is needed for index lookup
		//$result = mysqli_query($conn, "SHOW INDEX FROM `{$table}` WHERE KEY_NAME LIKE '%PRIMARY%'"); 1.1.15 updated
		$result = $db->query("SHOW INDEX FROM `{$table}`");
		if ($result->num_rows > 0) { 
			while ($row = $result->fetch_assoc()) { 
				$fields[] = $row['Column_name']; 
			} 
		} 
	
		return (count($fields) > 0) ? $fields : null;
	}

	/**
	 * LOAD
	 * Begins the processing for replace logic
	 * @param mysql  $conn			The db connection object
	 * @param array  $list			Key value pair of 'search' and 'replace' arrays
	 * @param array  $tables		The tables we want to look at
	 * @param array  $fullsearch    Search every column reguardless of its data type
	 * @return array Collection of information gathered during the run.
	 */
	public static function load($db, $list = array(), $tables = array(), $fullsearch = false) 
	{
		$report = array(
			'scan_tables' => 0, 
			'scan_rows' => 0, 
			'scan_cells' => 0,
			'updt_tables' => 0, 
			'updt_rows' => 0, 
			'updt_cells' => 0,
			'errsql' => array(), 
			'errser' => array(), 
			'errkey' => array(),
			'errsql_sum' => 0, 
			'errser_sum' => 0, 
			'errkey_sum' => 0,
			'time' => '', 
			'err_all' => 0
		);
		
		$walk_function = create_function('&$str', '$str = "`$str`";');

		$profile_start = time();
		if (is_array($tables) && !empty($tables)) {
			
			foreach ($tables as $table) 
			{
				$report['scan_tables']++;
				$columns = array();

				// Get a list of columns in this table
				$fields = $db->query('DESCRIBE ' . $table);
				while ($column = $fields->fetch_array()) {
					$columns[$column['Field']] = $column['Key'] == 'PRI' ? true : false;
				}

				// Count the number of rows we have in the table if large we'll split into blocks
				$row_count = $db->query("SELECT COUNT(*) FROM `{$table}`");
				$rows_result = $row_count->fetch_array();
				@$row_count->free();
				$row_count = $rows_result[0];
				if ($row_count == 0) {
					// Row 0 count to log
					continue;
				}

				$page_size = 25000;
				$offset = ($page_size + 1);
				$pages = ceil($row_count / $page_size);
				
				// Grab the columns of the table.  Only grab text based columns because 
				// they are the only data types that should allow any type of search/replace logic
				$colList = '*';
				$colMsg  = '*';
				if (! $fullsearch) 
				{
					$colList = self::getTextColumns($db, $table);
					if ($colList != null && is_array($colList)) {
						array_walk($colList, $walk_function);
						$colList = implode(',', $colList);
					} 
					$colMsg = (empty($colList)) ? '*' : '~';
				}
				
				if (empty($colList)) 
				{
					continue;
				} 
				

				//Paged Records
				for ($page = 0; $page < $pages; $page++) 
				{
					$current_row = 0;
					$start = $page * $page_size;
					$end   = $start + $page_size;
					$sql = sprintf("SELECT {$colList} FROM `%s` LIMIT %d, %d", $table, $start, $offset);
					$data  = $db->query($sql);

					if (!$data)
						$report['errsql'][] = $db->error;
					
					$scan_count = ($row_count < $end) ? $row_count : $end;
					//DUPX_Log::Info("\tScan => {$start} of {$scan_count}", 2);

					//Loops every row
					while ($row = $data->fetch_array()) 
					{
						$report['scan_rows']++;
						$current_row++;
						$upd_col = array();
						$upd_sql = array();
						$where_sql = array();
						$upd = false;
						$serial_err = 0;

						//Loops every cell
						foreach ($columns as $column => $primary_key) 
						{
							$report['scan_cells']++;
							$edited_data = $data_to_fix = $row[$column];
							$base64coverted = false;
							$txt_found = false;

							//Only replacing string values
							if (!empty($row[$column]) && !is_numeric($row[$column])) 
							{
								//Base 64 detection
								if (base64_decode($row[$column], true)) 
								{
									$decoded = base64_decode($row[$column], true);
									if (self::is_serialized($decoded)) 
									{
										$edited_data = $decoded;
										$base64coverted = true;
									}
								}
								
								//Skip table cell if match not found
								foreach ($list as $item) 
								{
									if (strpos($edited_data, $item['search']) !== false) {
										$txt_found = true;
										break;
									}
								}
								if (! $txt_found) {
									continue;
								}

								//Replace logic - level 1: simple check on any string or serlized strings
								foreach ($list as $item) {
									$edited_data = self::recursive_unserialize_replace($item['search'], $item['replace'], $edited_data);
								}

								//Replace logic - level 2: repair serilized strings that have become broken
								$serial_check = self::fix_serial_string($edited_data);
								if ($serial_check['fixed']) 
								{
									$edited_data = $serial_check['data'];
								} 
								elseif ($serial_check['tried'] && !$serial_check['fixed']) 
								{
									$serial_err++;
								}
							}

							//Change was made
							if ($edited_data != $data_to_fix || $serial_err > 0) 
							{
								$report['updt_cells']++;
								//Base 64 encode
								if ($base64coverted) {
									$edited_data = base64_encode($edited_data);
								}
								$upd_col[] = $column;
								$upd_sql[] = $column . ' = "' . $db->real_escape_string($edited_data) . '"';
								$upd = true;
							}

							if ($primary_key) {
								$where_sql[] = $column . ' = "' . $db->real_escape_string($data_to_fix) . '"';
							}
						}

						//PERFORM ROW UPDATE
						if ($upd && !empty($where_sql)) 
						{
							$sql = "UPDATE `{$table}` SET " . implode(', ', $upd_sql) . ' WHERE ' . implode(' AND ', array_filter($where_sql));
							$result = $db->query($sql) or $report['errsql'][] = $db->error;
							//DEBUG ONLY:
							//DUPX_Log::Info("\t{$sql}\n", 3);
							if ($result) {
								if ($serial_err > 0) {
									$report['errser'][] = "SELECT " . implode(', ', $upd_col) . " FROM `{$table}`  WHERE " . implode(' AND ', array_filter($where_sql)) . ';';
								}
								$report['updt_rows']++;
							}
						} elseif ($upd) {
							$report['errkey'][] = sprintf("Row [%s] on Table [%s] requires a manual update.", $current_row, $table);
						}
					}
					//DUPX_Util::fcgi_flush();
					@$data->free();
				}

				if ($upd) {
					$report['updt_tables']++;
				}
			}
		}
		$profile_end = time();		
		$report['errsql_sum'] = empty($report['errsql']) ? 0 : count($report['errsql']);
		$report['errser_sum'] = empty($report['errser']) ? 0 : count($report['errser']);
		$report['errkey_sum'] = empty($report['errkey']) ? 0 : count($report['errkey']);
		$report['err_all'] = $report['errsql_sum'] + $report['errser_sum'] + $report['errkey_sum'];
		return $report;
	}

	/**
	 * Take a serialised array and unserialise it replacing elements and
	 * unserialising any subordinate arrays and performing the replace.
	 * @param string $from       String we're looking to replace.
	 * @param string $to         What we want it to be replaced with
	 * @param array  $data       Used to pass any subordinate arrays back to in.
	 * @param bool   $serialised Does the array passed via $data need serialising.
	 * @return array	The original array with all elements replaced as needed. 
	 */
	public static function recursive_unserialize_replace($from = '', $to = '', $data = '', $serialised = false) 
	{
		// some unseriliased data cannot be re-serialised eg. SimpleXMLElements
		try 
		{
			if (is_string($data) && ($unserialized = @unserialize($data)) !== false) 
			{
				$data = self::recursive_unserialize_replace($from, $to, $unserialized, true);
			} 
			elseif (is_array($data)) 
			{
				$_tmp = array();
				foreach ($data as $key => $value) 
				{
					$_tmp[$key] = self::recursive_unserialize_replace($from, $to, $value, false);
				}
				$data = $_tmp;
				unset($_tmp);
				
				/* CJL
					Check for an update to the key of an array e.g.   [http://localhost/projects/wpplugins/] => 1.41
					This could have unintended consequences would need to enable with full-search needs more testing
				if (array_key_exists($from, $data)) 
				{
					$data[$to] = $data[$from];
					unset($data[$from]);
				}*/				
				
			} 
			elseif (is_object($data)) 
			{
				/* RSR Old logic that didn't work with Beaver Builder - they didn't want to create a brand new 
				object instead reused the existing one... 
				$dataClass = get_class($data);
				$_tmp = new $dataClass();
				foreach ($data as $key => $value) {
					$_tmp->$key = self::recursive_unserialize_replace($from, $to, $value, false);
				}
				$data = $_tmp;
				unset($_tmp);*/
				
				// RSR NEW LOGIC
				$_tmp = $data; 
				$props = get_object_vars( $data );
				foreach ($props as $key => $value) 
				{
					$_tmp->$key = self::recursive_unserialize_replace( $from, $to, $value, false );
				}
				$data = $_tmp;
				unset($_tmp);
			} 

			else 
			{
				if (is_string($data)) {
					$data = str_replace($from, $to, $data);
				}
			}

			if ($serialised)
				return serialize($data);
			
		} 
		catch (Exception $error) 
		{
			// Log some recursive unserialize error
		}
		return $data;
	}

	/**
	 *  IS_SERIALIZED
	 *  Test if a string in properly serialized */
	public static function is_serialized($data) 
	{
		$test = @unserialize(($data));
		return ($test !== false || $test === 'b:0;') ? true : false;
	}

	/**
	 *  FIX_STRING
	 *  Fixes the string length of a string object that has been serialized but the length is broken
	 *  @param string $data	The string ojbect to recalculate the size on.
	 *  @return 
	 */
	public static function fix_serial_string($data) 
	{
		$result = array('data' => $data, 'fixed' => false, 'tried' => false);
		if (preg_match("/s:[0-9]+:/", $data)) 
		{
			if (!self::is_serialized($data)) 
			{
				$regex = '!(?<=^|;)s:(\d+)(?=:"(.*?)";(?:}|a:|s:|b:|d:|i:|o:|N;))!s';
				$serial_string = preg_match('/^s:[0-9]+:"(.*$)/s', trim($data), $matches);
				//Nested serial string
				if ($serial_string) 
				{
					$inner = preg_replace_callback($regex, 'DUPX_UpdateEngine::fix_string_callback', rtrim($matches[1], '";'));
					$serialized_fixed = 's:' . strlen($inner) . ':"' . $inner . '";';
				} 
				else 
				{
					$serialized_fixed = preg_replace_callback($regex, 'DUPX_UpdateEngine::fix_string_callback', $data);
				}
				
				if (self::is_serialized($serialized_fixed)) 
				{
					$result['data'] = $serialized_fixed;
					$result['fixed'] = true;
				}
				$result['tried'] = true;
			}
		}
		return $result;
	}

	private static function fix_string_callback($matches) 
	{
		return 's:' . strlen(($matches[2]));
	}

}

//Remove trailing slashes
function _dupx_array_rtrim(&$value) {
    $value = rtrim($value, '\/');
}

function unset_safe_path($path) {
        return str_replace("/", "\\", $path);
    }

function _pre($var){
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}


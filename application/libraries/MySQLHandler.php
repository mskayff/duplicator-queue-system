<?php
include_once (dirname(__FILE__) . DIRECTORY_SEPARATOR . 'HttpHandler.php');

class MySQLHandler extends HttpHandler {
	
	/**
	 * 
	 * @param $xmlapi - A valid Xmlapi already initiated object
	 * @param $cpanel_user
	 */
	public function __construct($xmlapi, $cpanel_user){
		parent::__construct($xmlapi, $cpanel_user);
	}


	/**
	 *
	 * @param $dbname string
	 * @param $remove_existing_database boolean
	 */
	public function createDatabase($dbname, $remove_existing_database=false){
		// First check if the database exists
		$response = $this->xmlapi->api2_query($this->cpanel_user, 'MysqlFE', 'getalldbsinfo');

		$dataSet = $this->getObjectResponse($response);
		// ----------------------------------------------------------------------------------------
		// Check current databases in this cPanel
		// ----------------------------------------------------------------------------------------
		foreach ($dataSet as $dbinfo){
			// Check if the database already exists
			if ($dbinfo->db == $dbname && $remove_existing_database){
				// Remove existing database first
				$this->xmlapi->api2_query($this->cpanel_user, 'MysqlFE', 'deletedb', array('db' => $dbname));
				break;
			}
			
			if ($dbinfo->db == $dbname && !$remove_existing_database){
				throw new MySQLHandlerException ("The database {$dbname} already exists");
			}
		}

		$response = $this->xmlapi->api2_query($this->cpanel_user, "MysqlFE", "createdb", array('db' => $dbname));

		$response = $this->getObjectResponse($response);

		if(isset($response[0])){
			return $response[0];
		}
		return false;
	}

	public function createUser($username,$password){
		$response = $this->xmlapi->api2_query($this->cpanel_user, "MysqlFE", "createdbuser", array('dbuser' => $username, 'password' => $password));

		$response = $this->getObjectResponse($response);

		if(isset($response[0])){
			return $response[0];
		}
		return false;	
	}

	public function setUserPrivileges($db, $user, $privileges){
		$response = $this->xmlapi->api2_query($this->cpanel_user, "MysqlFE", "setdbuserprivileges", array(
				'db' => $db,
				'dbuser' => $user, 
				'privileges' => implode(",", $privileges)));

		$response = $this->getObjectResponse($response);

		if(isset($response[0])){
			return $response[0];
		}
		return false;
	}


	public function test(){


		// First check if the database exists
		$response = $this->xmlapi->api2_query($this->cpanel_user, 'MysqlFE', 'getalldbsinfo');

		$response = $this->getObjectResponse($response);

		print $this->_pre($response);
	}

	private function _pre($var){
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}

class MySQLHandlerException extends Exception{}
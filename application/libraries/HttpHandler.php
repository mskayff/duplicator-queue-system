<?php

abstract class HttpHandler {

	protected $xmlapi;
	protected $cpanel_user;
	protected $error = "";

	public function __construct($xmlapi, $cpanel_user){
		$this->xmlapi = $xmlapi;
		$this->cpanel_user = $cpanel_user;
	}

	protected function getJSONResponse($response){
		return $response->getResponse('JSON');
	}

	/**
	 *
	 * @throws HttpHandlerException
	 */
	public function getObjectResponse($response){
		$json = $this->getJSONResponse($response);

		// Decode the response
		$jResponse = json_decode($json);

		if(isset($jResponse->cpanelresult->error)){
			$this->error = $jResponse->cpanelresult->error;
			throw new HttpHandlerException($this->error);
		}

		return isset($jResponse->cpanelresult->data)? $jResponse->cpanelresult->data : $jResponse->cpanelresult;
	}

	public function getError(){
		return $this->error;
	}
	
}

class HttpHandlerException extends Exception{}
<?php

class FTPHandler extends HttpHandler{

	private $conn_id = null;
	private $domain = "";
	private $ftp_username;
	private $ftp_password;
	private $docroot = "";
	
	/**
	 * 
	 * @param $xmlapi - A valid Xmlapi already initiated object
	 * @param $cpanel_user
	 */
	public function __construct($domain, $xmlapi, $cpanel_user){
		parent::__construct($xmlapi, $cpanel_user);

		$this->domain = trim($domain);
	}

	/**
	 *
	 * Gets the root domain for a certain cpanel connection
	 *
	 */
	
	public function getRootDomain($set_as_domain=false){
		$response = $this->xmlapi->api2_query($this->cpanel_user, 'DomainLookup', 'getmaindomain'); 

		$response = $this->getObjectResponse($response);

		if(isset($response[0])){
			$domain = $response[0]->main_domain;

			if($set_as_domain){
				$this->domain = $domain;
			}
			return $domain;
		}
		return false;
	}


	public function setDomain($domain){
		$this->domain = $domain;
	}

	public function getRelDocRoot(){
		$response = $this->xmlapi->api2_query($this->cpanel_user, 'DomainLookup', 'getdocroot', array('domain' => $this->domain)); 

		$response = $this->getObjectResponse($response);

		if(isset($response[0])){
			$root = $response[0];

			$this->docroot = $root->docroot;

			return $root;
		}
		return false;
	}

	/**
	 *
	 * @throws HttpHandlerException
	 */
	public function createFtpUser($prefix){

		if(empty($this->domain))
			throw new FTPHandlerException("The domain is empty");

		$docRoots = $this->getRelDocRoot();

		if($docRoots){
			$this->ftp_username = $prefix . random_alpha(4);
			$this->ftp_password = generate_random_password(array(
						'length' => 14,
						'alpha' => 'on',
						'alpha_upper' => 'on',
						'numeric' => 'on',
						'special' => 'on'
					));

			// Call the xmlapi to create a new FTP user
			$response = $this->xmlapi->api2_query($this->cpanel_user, 'Ftp', 'addftp', array(
					'user' => $this->ftp_username,
					'pass' => $this->ftp_password,
					'quota' => 0,
					'homedir' => $docRoots->reldocroot
				));

			return $this->getObjectResponse($response);
		}

	}


	/**
	 * Retrieve the Ftp credentials
	 */
	public function getFtpCredentials(){
		return array(
				'user' => $this->ftp_username,
				'psw'  => $this->ftp_password
			);
	}

	/**
	 * Set the FTP Credentials
	 */
	public function setCredentials($user,$psw){
		$this->ftp_username = trim($user);
		$this->ftp_password = trim($psw);
	}


	/**
	 * Upload a file
	 * @param string $local - The local file path
	 * @param string $remote_file - The remote file name. Will be uploaded relative to the docroot
	 * @throws FTPHandlerException
	 */
	public function uploadFile($local, $remote_file = "", $mode = 0){

		if(!$this->conn_id){
			$this->connect();
		}

		//$r_file = $this->docroot . '/' . $remote_file;		

		// The ftp handles the remote directory implicitly based on the 
		// ftp user home directory
		if(ftp_put($this->conn_id, $remote_file, $local,  FTP_BINARY)){
			if($mode){
				ftp_chmod ($this->conn_id, $mode, $remote_file);
			}
			return true;
		}
		
		throw new FTPHandlerException("Failed to upload file");		
	}


	public function removeFile($file){
		if(!$this->conn_id){
			$this->connect();
		}

		if(ftp_delete($this->conn_id, $file)){
			return true;
		}
		return false;
	}


	/**
	 * Private Methods
	 */


	/**
	 * Connect to the ftp server
	 * @throws FTPHandlerException
	 */
	private function connect(){

		if(empty($this->domain)){
			throw new FTPHandlerException("Domain is empty. Unable to connect");
		}

		if(!($this->conn_id = ftp_connect($this->domain, 21))){
			Logger::log("queue", "Could not ftp connect with " . $this->domain);
			throw new FTPHandlerException("Could not FTP connect. FtpHandler::connect()");
		}

		if(empty($this->ftp_username) || empty($this->ftp_password))
			throw new FTPHandlerException("Empty FTP credentials");			

		// ----------------------------------------------------------------------------
		// Check if current username does have a @ just use this username
		// If it does not have the @, build a compound of the username and domain
		// ----------------------------------------------------------------------------
		$ftp_username = (strpos($this->ftp_username,"@") !== false)? $this->ftp_username :  $this->ftp_username .'@' . $this->domain;

		// iniciar una sesión con nombre de usuario y contraseña
		$login_result = @ftp_login($this->conn_id, $ftp_username, $this->ftp_password);
		// ----------------------------------------------------------------------------------------
		// Use passive mode.
		// ----------------------------------------------------------------------------------------
		ftp_pasv($this->conn_id, true);

		if(!$login_result){
			throw new FTPHandlerException("FTP Login has failed");
		}
	}

	/**
	 *
	 * Deletes an specific FTP user
	 *
	 */
	
	public function deleteFTPUser($user, $domain){
		if(empty($user)){
			throw new FTPHandlerException("Delete FTP user. A user must be provided.");
		}
		/*
		// ----------------------------------------------------------------------------
		// Check if username does have a @ just use this username
		// If it does not have the @, build a compound of the username and domain
		// ----------------------------------------------------------------------------
		$ftp_username = (strpos($user,"@") !== false)? $user :  $user .'@' . $domain;
		*/
		$root_domain = $this->getRootDomain();
		// ----------------------------------------------------------------------------------------
		// Remove the FTP account
		// ----------------------------------------------------------------------------------------
		$response = $this->xmlapi->api2_query($this->cpanel_user, 'Ftp', 'delftp', [
			'user' => $user,
			'domain' => $root_domain,
			// ----------------------------------------------------------------------------------------
			// Do not destry the ftp folder
			// ----------------------------------------------------------------------------------------
			'destroy' => 0
		]);

		$res = $this->getObjectResponse($response);		
		
		if(!$res || !$res[0]->result){
			return false;
		}

		return true;
	}

	public function close(){
		if($this->conn_id){
			ftp_close($this->conn_id);
			$this->conn_id = null;
		}
	}

	public function __destruct(){
		$this->close();
	}

}


class FTPHandlerException extends Exception {}
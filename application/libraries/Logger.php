<?php

class Logger {

	public static function log($tag, $msg){

		$dirname = LOGS_DIR . DIRECTORY_SEPARATOR .date('Y-m-d');

		if(!is_dir( $dirname)){
			if(!mkdir($dirname)){
				$dirname = LOGS_DIR;
			}
		}
		// Create the log filename
		$log_file_name = $dirname . DIRECTORY_SEPARATOR . $tag . '.log'; 

	  	// Open file
	    $fp = fopen($log_file_name, "a+");

	    if($fp){
	    	if(trim($msg) == "-")
	    		$logMsg = PHP_EOL;
	    	else
	    		$logMsg = "[" . date("Y-m-d H:i:s") . "]" . " - " . $msg . PHP_EOL;

	    	fwrite($fp,$logMsg);
	    	fclose($fp);
		}
	}

	/**
	 * Show Memory usage
	 * Display Memory usage in the unit passed in the method
	 * @param string $unit - The memory size unit - 'k','m',g'
	 */
	public static function showMemoryUsage($unit){

		$display = "";

		$bytes = memory_get_usage();

		switch(strtolower($unit)){
			case 'k' : $display = ( $bytes/1024 ) . ' KB'; break;
			case 'm' : $display = ($bytes/1048576) . ' MB' ; break; 
			case 'g' : $display = ($bytes/1.099e12) . ' GB' ; break;
		}

		echo $display . '<br>';
	}

	public static function pre($var){
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}

}

<?php

class PackageManager{

	private $package_file = "";

	public function __construct($package_file){
		$this->package_file = $package_file;
	}

	public function upgradeWP(){

		/*=====================================================
		=            Extract the wp version to tmp            =
		=====================================================*/
		// ---------------------------------------------------------------
		// The file path for the wp zipped version package we have
		// ---------------------------------------------------------------
		$new_pack_file = "";
		$d = opendir(WP_VERSION_UPDATE_DIR);
		// -----------------------------------------------------------------------
		// Iterate over the available files under the WP_VERSION_UPDATE_DIR
		// We expect just one file here
		// -----------------------------------------------------------------------
		while(false !== ($file = readdir($d))){
			$file_path = WP_VERSION_UPDATE_DIR . DIRECTORY_SEPARATOR . $file;
			if(is_file($file_path)){
				$new_pack_file = $file_path;
				// ---------------------------------------------------
				// Break the loop if we find a file
				// ---------------------------------------------------
				break;
			}
		}
		// ---------------------------------------------------
		// Close the directory
		// ---------------------------------------------------
		closedir($d);
		// ---------------------------------------------------
		// Check we actually found a file
		// ---------------------------------------------------
		if(empty($new_pack_file)){
			throw new PackageManagerException("No new wordpress version package found");
		}
		// ---------------------------------------------------
		// Check the file has a valid zip extension
		// ---------------------------------------------------
		if(pathinfo($new_pack_file, PATHINFO_EXTENSION) != "zip"){
			throw new PackageManagerException("The wordpress new version package is not a zip file!");
		}
		// ---------------------------------------------------
		// Open the Wordpress version zipped archive
		// to a temporal location
		// ---------------------------------------------------
		$zip = new ZipArchive;
		if($zip->open($new_pack_file) !== TRUE){
			throw new PackageManagerException("Could not open the wp version zip file");
		}	
		$zip->extractTo(TMP_DIR);
		$zip->close();	

		/*=====  End of Extract the wp version to tmp  ======*/
		

		/*=========================================================
		=            Remove files from current package            =
		= Current package is the package we uploaded for this     =
		= element in the queue.									  =
		=========================================================*/		
		$pkgZip = new ZipArchive;
		if(!file_exists(PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR . $this->package_file)){
			throw new PackageManagerException("The package file {$this->package_file} does not exist");
		}

		$zip_open_res = $pkgZip->open(PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR . $this->package_file);

		if($zip_open_res !== TRUE){
			throw new PackageManagerException("Could not open the zip package file. Error: {$zip_open_res}");
		}

		// ----------------------------------------------------------------------------------------
		// The criteria we are going to use here is the following:
		// ----------------------------------------------------------------------------------------
		// - We are going to kep the files in $dont_touch_files
		// - We are going to keep the files under wp-content
		// - We are going to remove the files under $force_remove_files
		// - We are going to remove the plugins at the $remove_plugins collection
		// - We are going to keep any other file
		// ----------------------------------------------------------------------------------------

		// ---------------------------------------------------
		// Don't touch these files
		// ---------------------------------------------------
		$dont_touch_files = [
			'database.sql',
			'installer-backup.php',
			'wp-config.php',	
			'robots.txt'		
		];
		// ----------------------------------------------------------------------------------------
		// Force remove this files (non wp- starting files)
		// ----------------------------------------------------------------------------------------
		$force_remove_files = [
			'index.php',
			'xmlrpc.php',
		];

		$remove_plugins = [
			'internal-links',
		];
		// ---------------------------------------------------
		// Iterate through every file in the package:
		// ---------------------------------------------------
		for ($i = 0; $i < $pkgZip->numFiles; $i++) {
		     // ----------------------------------------------------------------------------------------
		     // Get the filename at this index
		     // ----------------------------------------------------------------------------------------
		     $filename = $pkgZip->getNameIndex($i);
		     // ---------------------------------------------------
		     // Skip don't touch files
		     // ---------------------------------------------------
		     if(in_array($filename, $dont_touch_files)) continue;
		     // ---------------------------------------------------
		     // Check if under wp-content
		     // ---------------------------------------------------
		     if(strpos($filename, 'wp-content/') !== FALSE){
		     	// ----------------------------------------------------------------------------------------
		     	// Certain files under wp-content, won't be skipped for removal
		     	// for instance the plugin files under $remove_plugins
		     	// ----------------------------------------------------------------------------------------
		     	$skip = true;

		     	foreach ($remove_plugins as $plg) {
		     		// ----------------------------------------------------------------------------------------
		     		// Check if this is one of the plugins we wanna remove
		     		// ----------------------------------------------------------------------------------------
		     		if(preg_match("/^wp\-content\/plugins\/{$plg}/", $filename)){
		     			$skip=false;
		     			break;
		     		}
		     	}

		     	if($skip)
		     		continue;
		     }		     	

		     // ---------------------------------------------------
		     // Delete file 
		     // If filename is in the force_remove_files array
		     // or if starts with 'wp-*' then remove it
		     // ---------------------------------------------------
		     if(in_array($filename, $force_remove_files) || preg_match('/^wp\-\w+/', $filename)){
		     	//print "Removing: {$filename}" . PHP_EOL;
		     	$pkgZip->deleteIndex($i);
		     }
		}
		
		/*=====  End of Remove files from current package  ======*/


		/*==============================================================
		=            Copy wp version files into the package            =
		==============================================================*/
		$wp = opendir(TMP_DIR . DIRECTORY_SEPARATOR . 'wordpress');

		while(false !== ($file = readdir($wp))){
			// -------------------------------------------------------------------
			// Skip this directory and parent directory entries
			// -------------------------------------------------------------------
			if(in_array($file, ['.', '..']))
				continue;
			// ----------------------------------------------------------------------------------------
			// Current file in wordpress directory
			// ----------------------------------------------------------------------------------------
			$file_path = TMP_DIR . DIRECTORY_SEPARATOR . 'wordpress'. DIRECTORY_SEPARATOR . $file;
			// ----------------------------------------------------------------------------------------
			// Get the file base name
			// ----------------------------------------------------------------------------------------
			$file_base_name = pathinfo($file_path, PATHINFO_BASENAME);
			// ----------------------------------------------------------------------------------------
			// Check if file is valid and not a don't touch file
			// ----------------------------------------------------------------------------------------
			if(is_file($file_path) && !in_array($file_base_name, $dont_touch_files)){
				// ---------------------------------------------------
				// Copy this file to the zip package
				// ---------------------------------------------------
				$pkgZip->addFile($file_path, $file_base_name);
			}
			// ---------------------------------------------------
			// Process the wp-admin and wp-includes
			// ---------------------------------------------------
			else if(is_dir($file_path) && in_array($file_base_name, ['wp-admin', 'wp-includes'])){
				// ---------------------------------------------------
				// Copy this file to the zip package
				// ---------------------------------------------------
				$this->addFolderToZip($file_path . '/', $pkgZip, $file_base_name . '/');
			}
		}
		// ---------------------------------------------------
		// Add the htaccess file
		// ---------------------------------------------------	
		$pkgZip->addFile(APPPATH . 'templates' . DIRECTORY_SEPARATOR . 'htaccess-template.txt', '.htaccess');

		closedir($wp);
				
		/*=====  End of Copy wp version files into the package  ======*/

		$pkgZip->close();		

		// ---------------------------------------------------
		// Remove the temporal wordpress installation
		// ---------------------------------------------------
		echo exec ('rm -rf ' .  TMP_DIR . '/wordpress');

		return true;
	}


	public function updateWPConfigSalts($package_name){
		try {
			// ----------------------------------------------------------------------------------------
			// Build new ZipArchive object
			// ----------------------------------------------------------------------------------------
			$zipper = new ZipArchive;
			// ----------------------------------------------------------------------------------------
			// Open the zip package
			// ----------------------------------------------------------------------------------------
			$zipper->open(PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR . $package_name);
			// ----------------------------------------------------------------------------------------
			// Get the wp config contents
			// ----------------------------------------------------------------------------------------
			$wpconfig2 =  $zipper->getFromName('wp-config.php');
			$mem09 = "define( 'WP_MAX_MEMORY_LIMIT' , '3072M' );";
			$mem09rep = "";
			$wpconfig = str_replace($mem09,$mem09rep,$wpconfig2);
			
			// ----------------------------------------------------------------------------------------
			// Transform the wp-config into an array
			// ----------------------------------------------------------------------------------------
			$wp_config_array = preg_split("/\R/", $wpconfig);
			// ----------------------------------------------------------------------------------------
			// Get the WP Salts using this API call
			// ----------------------------------------------------------------------------------------
			$curl = new Curl("https://api.wordpress.org/secret-key/1.1/salt/");
			// ----------------------------------------------------------------------------------------
			// Salts from the WP API
			// ----------------------------------------------------------------------------------------
			$salts_raw = $curl->get();
			// ----------------------------------------------------------------------------------------
			// Convert the salts into an array entry
			// ----------------------------------------------------------------------------------------
			$salts = preg_split("/\R/", $salts_raw);


			// ----------------------------------------------------------------------------------------
			// Find the first SALTS occurrence, in the current wp-config which is AUTH_KEY
			// ----------------------------------------------------------------------------------------
			$begins_at = 0;
			for($i=0;$i<count($wp_config_array);$i++){
				$line = $wp_config_array[$i];

				if(preg_match("/'AUTH_KEY'/", $line)){
					$begins_at = $i;
				}
			}
			
			// ----------------------------------------------------------------------------------------
			// Replace each existing salt with the new salts.
			// ----------------------------------------------------------------------------------------
			foreach($salts as $salt){
				if(empty($salt))
					continue;
				$wp_config_array[$begins_at++] = $salt;
			}
			// ----------------------------------------------------------------------------------------
			// Write the wp-config back to the zip
			// ----------------------------------------------------------------------------------------
			$zipper->addFromString('wp-config.php', implode("\n", $wp_config_array));

			return true;

		} catch (Exception $e) {			
			return false;
		}
	}

	/**
	 *
	 * Function to recursively add a directory,
	 * sub-directories and files to a zip archive
	 */
	
	private function addFolderToZip($dir, $zipArchive, $zipdir = ''){
		if(!is_dir($dir))
			return false;
        if ($dh = opendir($dir)) {
            //Add the directory
            if(!empty($zipdir)) $zipArchive->addEmptyDir($zipdir);
          
            // Loop through all the files
            while (($file = readdir($dh)) !== false) {
          
                //If it's a folder, run the function again!
                if(!is_file($dir . $file)){
                    // Skip parent and root directories
                    if( ($file !== ".") && ($file !== "..")){
                        $this->addFolderToZip($dir . $file . "/", $zipArchive, $zipdir . $file . "/");
                    }
                  
                }else{
                    // Add the files
                    $zipArchive->addFile($dir . $file, $zipdir . $file);
                  
                }
            }
        }
	} 

	public static function detectPackage($dir, $domain){
		if(!is_dir($dir))
			throw new PackageManagerException("Wrong directory passed! {$dir}");

		// ----------------------------------------------------------------------------------------
		// Filter the domain. Remove dots, spaces, hyphens
		// ----------------------------------------------------------------------------------------
		$filtered_domain = preg_replace('/[\.\s\-_]*/', '', $domain);

		$d = opendir($dir);

		while(($pack = readdir($d)) !== false){
			if(preg_match("/{$filtered_domain}/", $pack)){
				return $pack;
			}
		}

		return false;
	}

}

class PackageManagerException extends Exception {}
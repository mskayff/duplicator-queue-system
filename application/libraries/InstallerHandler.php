<?php

class InstallerHandler {
	
	private $site_url = "";
	private $domain = "";
	private $old_url_root = "";
	private $old_doc_root = "";
	private $db_name = "";
	private $db_user = "";
	private $db_psw = "";
	private $remote_package = "";
	private $plugin_404_301 = false;

	public function __construct($queue){
		// Clean the domain name
		$this->domain = str_replace(array('http://', 'https://', 'www.'), "", $queue['domain']);
		$this->old_url_root = $queue["old_url"];
		$this->old_doc_root = $queue["old_doc_root"];
		$this->site_url = $queue['site_url'];
		$this->domain   = $queue['domain'];
		$this->db_name  = $queue['dbname'];
		$this->db_user  = $queue['db_user'];
		$this->db_psw   = $queue['db_psw'];
		$this->remote_package = REMOTE_DUPLICATOR_ZIP_FILENAME;
		// ---------------------------------------------------
		// Plugins
		// ---------------------------------------------------
		$this->plugin_404_301 		= $queue['plugin_404_301'];
		$this->remove_widget_titles = $queue["remove_widget_titles"];
		$this->allow_php_post_pages = $queue["allow_php_post_pages"];
		$this->flexi_pages_widget 	= $queue["flexi_pages_widget"];
		$this->new_random_post 		= $queue["new_random_post"];
	}

	public function createInstallerFile(){

		if(!file_exists(APPPATH . 'templates' . DIRECTORY_SEPARATOR . 'installer-template.php'))
			throw new InstallerHandlerException('Installer Template file not found');

		$installer_template = @file_get_contents(APPPATH . 'templates' . DIRECTORY_SEPARATOR . 'installer-template.php');

		$installer_buffer = str_replace('{{site_url}}', $this->site_url, $installer_template);
		$installer_buffer = str_replace('{{domain}}', $this->domain,     $installer_buffer);
		$installer_buffer = str_replace('{{old_url_root}}', $this->old_url_root ,$installer_buffer);
		$installer_buffer = str_replace('{{old_doc_root}}', $this->old_doc_root , $installer_buffer);
		$installer_buffer = str_replace('{{db_host}}', 'localhost',      $installer_buffer);
		$installer_buffer = str_replace('{{db_name}}', $this->db_name,   $installer_buffer);
		$installer_buffer = str_replace('{{db_user}}', $this->db_user,  $installer_buffer);
		$installer_buffer = str_replace('{{db_password}}', $this->db_psw, $installer_buffer);
		$installer_buffer = str_replace('{{package_name}}', $this->remote_package, $installer_buffer);
		// ---------------------------------------------------
		// Replace plugin keys
		// ---------------------------------------------------
		$installer_buffer = str_replace('{{plugin_404_301}}', 		($this->plugin_404_301? 'true' : 'false'), $installer_buffer);
		$installer_buffer = str_replace('{{remove_widget_titles}}', ($this->remove_widget_titles ? 'true' : 'false'),  $installer_buffer);
		$installer_buffer = str_replace('{{allow_php_post_pages}}', ($this->allow_php_post_pages ? 'true' : 'false'),  $installer_buffer);
		$installer_buffer = str_replace('{{flexi_pages_widget}}', 	($this->flexi_pages_widget ? 'true' : 'false'),  $installer_buffer);
		$installer_buffer = str_replace('{{new_random_post}}', 		($this->new_random_post ? 'true' : 'false'),  $installer_buffer);

		if(!is_writable(APPPATH . 'tmp')){
			@chmod(APPPATH . 'tmp', 0777);
		}

		$installer_script = APPPATH . 'tmp' . DIRECTORY_SEPARATOR . 'installer_' . $this->domain . '.php';

		$fp = fopen ($installer_script, 'w+');
		
		if(!$fp){
			return false;
		}

		fwrite($fp, $installer_buffer);
		fclose($fp);


		return $installer_script;
	}


	public function runInstall($installer_script_name){

		$curl = new Curl($this->site_url . '/' . $installer_script_name);
		$curl->connectionTimeout(60);
		$response = $curl->get(true);

		return $curl->getJSONResponse();
	}
}


class InstallerHandlerException extends Exception {}
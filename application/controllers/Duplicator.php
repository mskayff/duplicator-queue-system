<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'PackageManager.php');

class Duplicator extends MY_Controller {

	private $error = [];

	private $app_old_url_root;
	private $app_old_doc_root;
	
	public function __construct(){
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		$this->app_old_url_root = $this->db->get_where('config', array('option_name' => 'old_url_root'))->row()->option_value;
		$this->app_old_doc_root = $this->db->get_where('config', array('option_name' => 'old_doc_root'))->row()->option_value;
		$this->packages_folder  = $this->db->get_where('config', array('option_name' => 'packages_folder'))->row()->option_value;
	}


	public function index(){

		$query = $this->db->query('SELECT * FROM queue WHERE complete = 0 ORDER BY date_created ASC');

		$this->load->view('index', array(
				'queue' => $query->result()
			));
	}

	/**
	 * Add New package queue
	 *
	 */
	public function addNew(){

		$this->load->helper('date');		

		// Check if POST
		if($this->input->method(false) == "post"){
			// It's a POST. Process form.
			// ---------------------------
			// Catch the post data
			// ---------------------------
			$old_url_root 	= $this->input->post('old_url_root');
			$old_doc_root 	= $this->input->post('old_doc_root');
			$site_url 		= $this->input->post('site_url');
			$cpanel_url 	= $this->input->post('cpanel_url');
			$cpanel_user 	= $this->input->post('cpanel_user');
			$cpanel_psw 	= $this->input->post('cpanel_password');
			// ---------------------------------------------------
			// Plugins
			// ---------------------------------------------------
			$plugin_404 			= $this->input->post('plugin_404_301');
			$remove_widget_titles 	= $this->input->post("remove_widget_titles");
			$allow_php_post_pages 	= $this->input->post("allow_php_post_pages");
			$flexi_pages_widget 	= $this->input->post("flexi_pages_widget");
			$new_random_post 		= $this->input->post("new_random_post");

			// Valdiate Empty vars
			$this->_validateEmpty($old_url_root, "Old url root cannot be empty");
			$this->_validateEmpty($old_doc_root, "Old docroot cannot be empty");
			$this->_validateEmpty($site_url, "Site url cannot be empty");
			$this->_validateEmpty($cpanel_url, "cPanel url cannot be empt");
			$this->_validateEmpty($cpanel_user, "Cpanel user cannot be empt");
			$this->_validateEmpty($cpanel_psw, "cPanel password cannot be empty");
			$this->_validateEmpty($_FILES['package_file']['name'], "You must select a package file");			


			// ----------------------------
			// Validate the Site URL
			// ----------------------------			
			if(strpos($site_url, 'http') === FALSE){
				$this->error[] = "Site url must contain http:// or https://";
			}
			// ----------------------------------------------------------------------------------------
			// Filter the cpanel url
			// ----------------------------------------------------------------------------------------
			$cpanel_url = $this->filterCPanelUrl($cpanel_url);

			if(empty($this->error)){
				$this->load->library('upload', array(
					'upload_path' => PACKAGE_ZIP_UPLOAD_PATH,
					'allowed_types' => 'zip',					
					'overwrite' => true,
					'remove_spaces' => true
				));

				if (! $this->upload->do_upload('package_file')){
					$this->error[] = $this->upload->display_errors();
				}
				else{
					// Get the uploaded file info
					$package_data = $this->upload->data();

					$domain_name = parse_url($site_url,  PHP_URL_HOST);
					$domain_name = str_replace("www.", "", $domain_name);

					// Add to the queue
					$this->db->insert('queue', [
							'site_url' => trim($site_url),
							'domain'  => $domain_name, 
							'old_url' => $old_url_root,
							'old_doc_root' => $old_doc_root,
							'cpanel_url' => trim($cpanel_url),
							'cpanel_user' => trim($cpanel_user),
							'cpanel_psw' => trim($cpanel_psw),							
							'package' => $package_data['file_name'],
							'date_created' => date('Y-m-d H:i:s')
						]);

					// ---------------------------------------------------
					// Capture the queue id
					// ---------------------------------------------------
					$qid = $this->db->insert_id();

					$this->db->insert('queue_plugins', [
							'qid' => $qid,
							'plugin_404_301' => $plugin_404? 1 : 0,
							'remove_widget_titles' => $remove_widget_titles? 1 : 0,
							'allow_php_post_pages' => $allow_php_post_pages ? 1 : 0,
							'flexi_pages_widget'   => $flexi_pages_widget ? 1 : 0,
							'new_random_post'      => $new_random_post ? 1 : 0
						]);

					$this->session->set_flashdata('success', 'New package queued succesfully!');
					// Check this, must avoid 
					redirect('duplicator/index');			
				}
			}
		}
		$this->load->view('queue_new', array(
				'error' => $this->error,
				'old_url_root' => $this->app_old_url_root ,
				'old_doc_root' => $this->app_old_doc_root
			));
	}


	/**
	 * Edit Queue item method
	 *
	 */
	public function edit($qid){

		// Initialize an error variable
		$error = [];

		if(!is_numeric($qid)){
			$this->session->set_flashdata('error', 'Bad request for queue item edit.');
			redirect('duplicator/index');
		}

		// Check if POST
		if($this->input->method(false) == "post"){
			$is_file_upload = false;
			// ---------------------------
			// Catch the post data
			// ---------------------------
			$old_url_root = $this->input->post('old_url_root');
			$old_doc_root = $this->input->post('old_doc_root');
			$site_url    = $this->input->post('site_url');
			$cpanel_url  = $this->input->post('cpanel_url');
			$cpanel_user = $this->input->post('cpanel_user');
			$cpanel_psw  = $this->input->post('cpanel_password');
			$is_root_domain = $this->input->post('is_root_domain');
			// ---------------------------------------------------
			// Plugins info
			// ---------------------------------------------------
			$plugin_404 			= $this->input->post('plugin_404_301');
			$remove_widget_titles 	= $this->input->post("remove_widget_titles");
			$allow_php_post_pages 	= $this->input->post("allow_php_post_pages");
			$flexi_pages_widget 	= $this->input->post("flexi_pages_widget");
			$new_random_post 		= $this->input->post("new_random_post");

			$prefix   = $this->input->post('prefix');
			$dbname   = $this->input->post('db_name');
			$db_user  = $this->input->post('db_user');
			$db_psw   = $this->input->post('db_psw');
			$ftp_user = $this->input->post('ftp_user');
			$ftp_psw  = $this->input->post('ftp_psw');

			// Valdiate Empty vars
			$this->_validateEmpty($old_url_root, "Old url root cannot be empty");
			$this->_validateEmpty($old_doc_root, "Old docroot cannot be empty");
			$this->_validateEmpty($site_url, "Site url cannot be empty");
			$this->_validateEmpty($cpanel_url, "cPanel url cannot be empt");
			$this->_validateEmpty($cpanel_user, "Cpanel user cannot be empt");
			$this->_validateEmpty($cpanel_psw, "cPanel password cannot be empty");		

			// --------------------------
			// Validate the old url
			// --------------------------
			/*
			if(filter_var($old_url_root, FILTER_VALIDATE_URL) === FALSE){
				$this->error[] = "Invalid Old url root";
			}
			*/

			// ----------------------------
			// Validate the Site URL
			// ----------------------------			
			if(strpos($site_url, 'http') === FALSE){
				$this->error[] = "Site url must contain http:// or https://";
			}

			// ----------------------------------------------------------------------------------------
			// Filter the cpanel url
			// ----------------------------------------------------------------------------------------
			$cpanel_url = $this->filterCPanelUrl($cpanel_url);

			// ------------------------------------------------
			// Check if there's new package file to upload
			// ------------------------------------------------
			if($_FILES['package_file']['name'] != ""){
				$this->load->library('upload', array(
					'upload_path' => PACKAGE_ZIP_UPLOAD_PATH,
					'allowed_types' => 'zip',					
					'overwrite' => true,
					'remove_spaces' => true
				));

				if (!$this->upload->do_upload('package_file')){
					$this->error[] = $this->upload->display_errors();
				}
				else{
					$is_file_upload = true;
					// Get the uploaded file info
					$package_data = $this->upload->data();
				}
			}

			// --------------------
			// If no errors
			// --------------------
			if(empty($this->error)){
				
				$domain_name = str_replace("www.", "", parse_url($site_url,  PHP_URL_HOST));

				// No errors
				$queueEntity = new stdClass();
				$queueEntity->site_url = 	trim($site_url);
				$queueEntity->domain =  trim($domain_name);
				$queueEntity->old_url = 	trim($old_url_root);
				$queueEntity->old_doc_root = trim($old_doc_root);
				$queueEntity->cpanel_url = 	trim($cpanel_url);
				$queueEntity->cpanel_user = trim($cpanel_user);
				$queueEntity->cpanel_psw = 	trim($cpanel_psw);
				$queueEntity->is_root_domain = $is_root_domain? 1 : 0;
				if($is_file_upload){
					$queueEntity->package = $package_data['file_name'];
				}

				// Update the queue entity
				$upd = $this->db->where('id',$qid)
							 ->update('queue', $queueEntity);

				// ---------------------------------------------------
				// Update the plugins info and add/update the 
				// queue credentials
				// ---------------------------------------------------
				if($upd){
					// ---------------------------------------------------
					// Update the plugins data
					// ---------------------------------------------------
					$this->db->where('qid', $qid)
						->update('queue_plugins', [
							'plugin_404_301'	   => $plugin_404? 1 : 0,
							'remove_widget_titles' => $remove_widget_titles? 1 : 0,
							'allow_php_post_pages' => $allow_php_post_pages ? 1 : 0,
							'flexi_pages_widget'   => $flexi_pages_widget ? 1 : 0,
							'new_random_post'      => $new_random_post ? 1 : 0
						]);
					
					// ---------------------------------------------------
					// Update the credentials
					// ---------------------------------------------------
					$creds = array(
							'q_id' => $qid,
							'prefix' => $prefix,
							'dbname' => $dbname,
							'db_user' => $db_user,
							'db_psw' => $db_psw,
							'ftp_user' => $ftp_user,
							'ftp_psw' => $ftp_psw
						);
					$this->db->replace('queue_creds', $creds);

					$this->session->set_flashdata('success', 'Queue package updated sucesfully');
					redirect('duplicator/index');
				}else{
					$this->error[] = "Failed to update the queue package";
				}			
			}
		}

		/**
		 * This is a left join as it doesn't matter if the right table does
		 * not exist
		 */
		$query = $this->db->select('*')->from('queue')
					->join('queue_creds', 'queue.id=queue_creds.q_id', 'left')	
					->join('queue_plugins', 'queue.id=queue_plugins.qid', 'left')				
					->where('queue.id', $qid)
					->limit(1)
					->get();

		// Return the row item
		$queue = $query->row();

		if(!$queue){
			$this->session->set_flashdata('error', 'The queue package could not be retrieved');
			redirect('duplicator/index');
		}

		// ---------------------
		// Load the View
		// ---------------------
		$this->load->view('edit_queue', array(
				'show_back' => true,
				'queue' => $queue,
				'error' => $this->error
			));
	}


	/**
	 * Delete package
	 * @return JSON 
	 */
	public function ajx_delete(){

		$response = [
			'status' => 0,
			'msg' => ''];

		$qid = $this->input->post('qid');

		if (empty($qid)){
			$reponse['msg'] = 'Qid cannot be empty';
			$this->output
	        	->set_content_type('application/json')
	        	->set_output(json_encode($response));
	        return false;
		}

		$del = $this->db->delete('queue', array('id'=>$qid));

		if($del){
			// Remove was succesful
			$response['status'] = 200;
			$response['msg'] = "Queue item removed succesfully";
		}
		else{
			$response['msg'] = "Queue item could not be removed";
		}

		$this->output
        	->set_content_type('application/json')
        	->set_output(json_encode($response));
	}

	private function _validateEmpty($var, $msg){
		if(empty($var))
			$this->error[] = $msg;
	}



	/**
	 *
	 *
	 */
	public function config(){

		$packages_folder = $this->db->get_where('config', array('option_name' => 'packages_folder'))->row()->option_value;

		if ($this->input->method(false) == "post"){
			$this->app_old_url_root = $this->input->post("old_url_root");
			$this->app_old_doc_root = $this->input->post("old_doc_root");
			$packages_folder = $this->input->post('packages_folder');

			// Validate the url root
			if (!empty($this->app_old_url_root) && filter_var($this->app_old_url_root, FILTER_VALIDATE_URL) === FALSE){
				$this->error[] = "Old url is not a valid url";
			}

			if(empty($this->error)){

				// Update the configuration information
				$this->db->where('option_name', 'old_url_root')
						->update('config', array(
								'option_value' => $this->app_old_url_root
							));

				$this->db->where('option_name', 'old_doc_root')
						->update('config', array(
								'option_value' => $this->app_old_doc_root
							));

				$this->db->where('option_name', 'packages_folder')
					->update('config', [
						'option_value' => $packages_folder
					]);

				$this->session->set_flashdata('success', 'Config updated succesfully');
				redirect('duplicator/index');
			}
		}

		$this->load->view('config', [
				'old_url_root' 		=> $this->app_old_url_root ,
				'old_doc_root' 		=> $this->app_old_doc_root,
				'packages_folder' 	=> $packages_folder,
				'error' 			=> $this->error
			]);
	}


	public function test(){

		$query = $this->db->select('*')->from('queue')
					->join('queue_creds', 'queue.id=queue_creds.q_id', 'left')					
					->where('queue.id', 2)
					->limit(1)
					->get_compiled_select();

		echo $query;

		/*
		include_once (APPPATH . 'third_party/publicapi-php/Cpanel/Util/Autoload.php');
		include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'MySQLHandler.php');

		// -----------------------------
		// Build the cPanel handler
		// -----------------------------
		$config = array(
				'host' =>   'matrix3.hostthename.com',
				'user' =>	 'banj34',
				'password' => 'M4rkt3st$14324'
			);

		$xmlapi = Cpanel_PublicAPI::factory('WHM', $config);

		$mysql = new MySQLHandler( $xmlapi, $config['user']);

		$mysql->test();
		*/
	}


	/**
	 *
	 * CSV fromat 
	 * 
	 */	

	public function csvUpload(){
		// ---------------------------------------------------
		// The filename of the csv once uploaded
		// ---------------------------------------------------
		$desired_filename = "imported.csv";

		// ---------------------------------------------------
		// Do some primary verifications
		// ---------------------------------------------------
		if(empty($this->packages_folder)){
			return $this->jsonResponse([
				'status' => 'fail',
				'msg'	 => 'Alert! Packages folder is not set in the config'
			]);
		}

		// ---------------------------------------------------
		// Load the CodeIgniter Upload library
		// withe this configuration
		// ---------------------------------------------------
		$this->load->library('upload', [
			'upload_path' 		=> TMP_DIR,
			'allowed_types' 	=> 'csv',
			'overwrite'         => true, // Important! Overwrite, otherwise the Upload libary will name the uploaded file sequencially
			'file_name'     	=> $desired_filename,
			'file_ext_tolower'  => true
		]);

		try {
			// ----------------------------
			// Upload the csv file !!
			// ----------------------------
			if(!$this->upload->do_upload('csv')){
				return $this->jsonResponse([
					'status' => 'fail',
					'msg'    => $this->upload->display_errors()
				]);
			}
			$path = $this->upload->data('full_path');
			// ---------------------------------------------------
			// Open the CSV file!
			// ---------------------------------------------------
			$fp = fopen($path, 'r');

			if(!$fp){
				return $this->jsonResponse([
					'status' => 'fail',
					'msg'    => "Unable to open the file",
				]);
			}

			// ----------------------------------------------------------------------
			// The result.
			// We are going to retrieve this as the whole result
			// for this script process
			// ----------------------------------------------------------------------
			$result_set_csv = [];
			$succesful_count = 0;
			$counter=0;
			// ---------------------------------------------------
			// Process each and every line in the csv
			// ---------------------------------------------------
			while(($line = fgetcsv($fp)) != false){
				// ---------------------------------------------------
				// Skip the headers
				// Be aware we increment the counter here!
				// When the increment is applied after the variable
				// the comparison takes place first
				// ---------------------------------------------------
				if($counter++ == 0)
					continue;		

				// ----------------------------------------------------------------------------------------
				// Filter the cpanel url
				// ----------------------------------------------------------------------------------------
				$cpanel_url = $this->filterCPanelUrl($line[3]);

				// ---------------------------------------------------
				// Make it an associative array
				// ---------------------------------------------------
				$pack = [				
					"old_url" => $line[0],
					"old_doc_root" => $line[1],
					"site_url" => $line[2],
					"cpanel_url" => $cpanel_url,
					"cpanel_user" => $line[4],
					"cpanel_psw" => $line[5],				
					"ftp_user" => $line[6],
					"ftp_psw" => $line[7],				
					"plugin_404_301" => $line[8],
					"remove_widget_titles" => $line[9],
					"allow_php_post_pages" => $line[10],
					"flexi_pages_widget" => $line[11],
					"new_random_post" => $line[12],
					"is_root_domain" => strtolower($line[13]) == "y"? 1 : 0
				];
				// ---------------------------------------------------
				// Required fields in the csv file.
				// These fields must be present
				// ---------------------------------------------------
				$required = [
					'site_url',
					'cpanel_url',
					'cpanel_user',
					'cpanel_psw',
				];
				// ---------------------------------------------------
				// Errors container for the required field
				// ---------------------------------------------------
				$missing_required=[];
				// ---------------------------------------------------------------------------
				// Verify required fields are present for this record
				// ---------------------------------------------------------------------------
				foreach($required as $r){
					if(empty($pack[$r])){
						array_push($missing_required, $r);
					}
				}
				// ---------------------------------------------------
				// Flag this row as having missing information
				// ---------------------------------------------------
				if(!empty($missing_required)){
					array_push($result_set_csv, [
						'status' => 'fail',
						'line'   => $counter,
						'error'  => "Error in line {$counter}. Site: {$pack['site_url']}. Missing required fields " . implode(',', $missing_required)
					]);
					// ---------------------------------------------------
					// Continue if error
					// ---------------------------------------------------
					continue;
				}			

				/*============================================
				=            Package manipulation            =
				============================================*/
				$domain_name = parse_url($pack['site_url'],  PHP_URL_HOST);
				$domain_name = trim(str_replace("www.", "", $domain_name));
				// ----------------------------------------------------------------------------------------
				// Auto detect the package
				// ----------------------------------------------------------------------------------------
				$package_name = PackageManager::detectPackage($this->packages_folder, $domain_name);
				// ------------------------------------------------------------------
				// Verify the package original source file exists
				// ------------------------------------------------------------------
				if(!$package_name){
					array_push($result_set_csv, [
						'status' => 'fail',
						'line'   => $counter,
						'error'  => "Error in line {$counter}. Site: {$pack['site_url']}. The package file cannot be found!"
					]);
					// ---------------------------------------------------
					// Continue if error
					// ---------------------------------------------------
					continue;
				}
				// ---------------------------------------------------
				// Append a slash if not present in the path name
				// ---------------------------------------------------
				if(!in_array(substr($this->packages_folder, -1), ["/", DIRECTORY_SEPARATOR, "\\"]))
					$this->packages_folder .= DIRECTORY_SEPARATOR;
				
				$package_source_path = $this->packages_folder . $package_name;						
				// ----------------------------------------------------------------------------------------------
				// Copy the package to the package path we have set in the config			
				// ----------------------------------------------------------------------------------------------
				copy($package_source_path, PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR . $package_name);

				/*=====  End of Package manipulation  ======*/					
				

				// ---------------------------------------------------
				// Add pack to the queue
				// ---------------------------------------------------
				$this->db->insert('queue', [
						'site_url' 	 	 => trim($pack['site_url']),
						'domain'  		 => $domain_name, 
						'old_url' 		 => $pack['old_url']? $pack['old_url'] : $this->app_old_url_root,
						'old_doc_root'   => $pack['old_doc_root']? $pack['old_doc_root'] : $this->app_old_doc_root,
						'cpanel_url' 	 => trim($pack['cpanel_url']),
						'cpanel_user'    => trim($pack['cpanel_user']),
						'cpanel_psw' 	 => trim($pack['cpanel_psw']),							
						'package' 		 => $package_name,
						'is_root_domain' => $pack['is_root_domain'],
						'date_created'   => date('Y-m-d H:i:s')
					]);
				// ---------------------------------------------------
				// Queued item id
				// ---------------------------------------------------
				$qid = $this->db->insert_id();
				// ---------------------------------------------------
				// Insert the plugins information
				// ---------------------------------------------------
				$this->db->replace('queue_plugins', [
						'qid' => $qid,
						'plugin_404_301' 	   => !empty($pack['plugin_404_301'])? 1 : 0,
						'remove_widget_titles' => !empty($pack['remove_widget_titles'])? 1 : 0,
						'allow_php_post_pages' => !empty($pack['allow_php_post_pages'])? 1 : 0,
						'flexi_pages_widget'   => !empty($pack['flexi_pages_widget'])? 1 : 0,
						'new_random_post'      => !empty($pack['new_random_post'])? 1 : 0,
					]);

				// ---------------------------------------------------
				// If FTP user and FTP password aren't empty
				// add new credentials record
				// ---------------------------------------------------
				if(!empty($pack['ftp_user']) && !empty($pack['ftp_psw'])){
					// ---------------------------------------------------
					// Insert the credentials information
					// ---------------------------------------------------
					$this->db->replace('queue_creds', [
						'q_id' => $qid,
						'prefix' => "",
						'dbname' => "",
						'db_user' => "",
						'db_psw' => "",
						'ftp_user' => $pack['ftp_user'],
						'ftp_psw' => $pack['ftp_psw'],
					]);
				}
				// ---------------------------------------------------
				// Add a succesful pack!!
				// ---------------------------------------------------
				array_push($result_set_csv, [
					'status' => 'ok',
					'line'   => $counter,
					'site_url'   => $pack['site_url'],
					'cpanel_url' => $pack['cpanel_url']
				]);
				// ---------------------------------------------------
				// Increment the succesful count
				// ---------------------------------------------------
				$succesful_count++;
			}

			if($counter <= 1){
				return $this->jsonResponse([
					'status' => 'fail',
					'msg'    => 'No items found in the CSV file.'
				]);
			}

			// ---------------------------------------------------
			// Final successful response
			// ---------------------------------------------------
			return $this->jsonResponse([
				'status' 			=> 'ok',
				'msg'    			=> 'Process completed',
				'success_count' 	=> $succesful_count,
				'failed_count'    	=> count($result_set_csv) - $succesful_count,
				'result' 			=> $result_set_csv
			]);


		} catch (Exception $e) {
			return $this->jsonResponse([
				'status' 			=> 'fail',
				'msg'    			=> $e->getMessage(),
			]);
		}		
	}
	

	// ---------------------------------------------------
	// Page for the import CSV upload
	// ---------------------------------------------------
	public function importCsv(){
		$this->load->view('import-csv',[
			'show_back' => true,
			'packages_folder_warning' => $this->packages_folder? false : true
		]);
	}


	/**
	 *
	 * Filter cpanel urls
	 *
	 */
	private function filterCPanelUrl($cpanel_url){
		// ----------------------------------------------------------------------------------------
		// Remove any leading http or https
		// ----------------------------------------------------------------------------------------
		if(preg_match('/^https?\:\/\//', $cpanel_url, $match)){
			$cpanel_url = str_replace($match[0], "", $cpanel_url);
		}
		// ----------------------------------------------------------------------------------------
		// Remove any trailing :2083 :2082 or :2087
		// ----------------------------------------------------------------------------------------
		if(preg_match('/\:208[237]$/', $cpanel_url, $match)){
			$cpanel_url = str_replace($match[0], "", $cpanel_url);
		}			
		
		return $cpanel_url;	
	}
	
}
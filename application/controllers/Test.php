<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once (APPPATH . 'third_party/publicapi-php/Cpanel/Util/Autoload.php');

include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'PackageManager.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'Curl.class.php');

class Test extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('queue_creds');
	}


	public function zipper(){
		$zipper = new ZipArchive;

		$zipper->open(PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR .'20171128_testcom_acfcffa1f0ac01815673171128041026_archive.zip');

		$wpconfig =  $zipper->getFromName('wp-config.php');

		$wp_config_array = preg_split("/\R/", $wpconfig);

		_pre($wp_config_array);

		$curl = new Curl("https://api.wordpress.org/secret-key/1.1/salt/");
		// ----------------------------------------------------------------------------------------
		// Salts from the WP API
		// ----------------------------------------------------------------------------------------
		$salts_raw = $curl->get();

		$salts = preg_split("/\R/", $salts_raw);

		// ----------------------------------------------------------------------------------------
		// Find the first occurrence which is AUTH_KEY
		// ----------------------------------------------------------------------------------------
		$begins_at = 0;
		for($i=0;$i<count($wp_config_array);$i++){
			$line = $wp_config_array[$i];

			if(preg_match("/'AUTH_KEY'/", $line)){
				$begins_at = $i;
			}
		}
		// ----------------------------------------------------------------------------------------
		// Replace each existing salt with the new salts.
		// ----------------------------------------------------------------------------------------
		foreach($salts as $salt){
			if(empty($salt))
				continue;
			$wp_config_array[$begins_at++] = $salt;
		}
		// ----------------------------------------------------------------------------------------
		// Write the wp-config back to the zip
		// ----------------------------------------------------------------------------------------
		$zipper->addFromString('wp-config.php', implode("\n", $wp_config_array));
	}


	public function queue($qid){
		// --------------------------------
		// Check qid is numeric
		// --------------------------------
		if(!is_numeric(intval($qid)) || $qid == 0){
			return $this->output->set_output("Bad Request. Missing queue id.");
		}

		// ----------------------------------------------------------
		// Left join for queue and credentials
		// Important! This is a left join as we require the full
		// set even if the table to the right does not exist
		// ----------------------------------------------------------
		$query = $this->db->select('*')->from('queue')
					->join('queue_creds', 'queue.id=queue_creds.q_id', 'left')					
					->where('queue.id', $qid)
					->limit(1)
					->get();

		// Return the row item
		$oQueue = $query->row();

		if(!$oQueue){
			return $this->output->set_output("Queue item not found");
		}

		// --------------------------------------------------------
		// Convert the queue result object from the db to an 
		// associative array
		// --------------------------------------------------------
		$queue = get_object_vars($oQueue);

		// -----------------------------
		// Build the cPanel handler
		// -----------------------------
		$xmlapi = Cpanel_PublicAPI::factory('WHM', array(
									'host' 	   =>   $queue['cpanel_url'],
									'user' 	   =>	$queue['cpanel_user'],
									'password' =>   $queue['cpanel_psw']
								));
		$xmlapi->setPort('2083');


		$result = $xmlapi->api2_query($queue['cpanel_user'], 'DomainLookup', 'getmaindomain');

		_pre($this->getObjectResponse($result));// ->cpanelresult->data
	}	



	public function wppacker (){
		$pm = new PackageManager("20171206_domaincom_5a274b2b1644f6316171206014307_archive.zip");
		$pm->upgradeWP();
	}


	public function ftp(){

		$conn_id = 0;

		if(!($conn_id = ftp_connect("whatsappleak.com"))){
			echo "Failed to ftp connect";
		}

		// Init ftp session
		$login_result = @ftp_login($conn_id, "wh4ts8ap_kxgr@whatsappleak.com", "bpXC^{[q)7$K");

		if(!$login_result){
			echo "Could not login<br>";
			return;
		}

	}

	public function preg_match(){
		$cpanel_url = "http://lv-shared04.cpanelplatform.com:2083";
		// ----------------------------------------------------------------------------------------
		// Remove any leading http or https
		// ----------------------------------------------------------------------------------------
		if(preg_match('/^https?\:\/\//', $cpanel_url, $match)){
			$cpanel_url = str_replace($match[0], "", $cpanel_url);
		}

		// ----------------------------------------------------------------------------------------
		// Remove any trailing :2083 :2082 or :2087
		// ----------------------------------------------------------------------------------------
		if(preg_match('/\:208[237]$/', $cpanel_url, $match)){
			$cpanel_url = str_replace($match[0], "", $cpanel_url);
		}

		print "{$cpanel_url}";
	}

	protected function getJSONResponse($response){
		return $response->getResponse('JSON');
	}

	/**
	 *
	 * @throws HttpHandlerException
	 */
	public function getObjectResponse($response){
		$json = $this->getJSONResponse($response);

		// Decode the response
		$jResponse = json_decode($json);

		if(isset($jResponse->cpanelresult->error)){
			$this->error = $jResponse->cpanelresult->error;
			throw new HttpHandlerException($this->error);
		}

		return isset($jResponse->cpanelresult->data)? $jResponse->cpanelresult->data : $jResponse->cpanelresult;
	}
}
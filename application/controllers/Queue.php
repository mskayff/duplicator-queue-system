<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once (APPPATH . 'third_party/publicapi-php/Cpanel/Util/Autoload.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'MySQLHandler.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'FTPHandler.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'InstallerHandler.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'Curl.class.php');
include_once (APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'PackageManager.php');

// -------------------------------------------------------
// Some constants useful in this controller/script
// -------------------------------------------------------
define ('INSTALLER_SCRIPT_NAME' , 'installer.php');
// -------------------------------------------------------
// Name of the duplicator package on the remote server
// -------------------------------------------------------
define ('REMOTE_DUPLICATOR_ZIP_FILENAME', 'duplicator-package.zip');



class Queue extends CI_Controller {

	private $error = "";

	private $avoid_package_upload_test = false;

	public function __construct(){
		parent::__construct();
		$this->load->model('queue_creds');
	}
	
	/**
	 * Get all details from current queue in json format
	 */
	public function ajx_queue_details(){

		$query = $this->db->order_by('date_created', 'ASC')
					->get_where('queue', array(
							'complete' => 0
						));

		$response = array(
				'count' => $query->num_rows(),
				'items' => []
			);

		foreach ($query->result() as $item) {
			$p = new stdClass();
			$p->id = $item->id;
			$p->name = $item->domain;
			$p->site_url = $item->site_url;
			array_push($response['items'], $p);
		}

		$this->output->set_content_type ('application/json')
				->set_output(json_encode($response));
	}



	/**
	 * Process an item in the queue
	 * POST params
	 * @param int qid
	 * @return JSON
	 */
	public function process_item(){
		// -----------------------------------------------------
		// Set a new time limit of 10 mins for this script
		// -----------------------------------------------------
		set_time_limit (600);
		// -----------------------
		// The JSON response
		// -----------------------
		$json_response = array(
			'status' => '000',				
			'msg'    => '',				
			'db'     => false,
			'dbname' => '',
			'db_user' => false,
			'db_user_priv' => false,
			'ftp_user' => false,				
			'package' => false,
			'install' => false,
			'ptime' => ''
		);		

		// ------------------
		// Starting time
		// ------------------
		$start_time = time();

		// --------------------------------
		// Prepare the output as json
		// --------------------------------
		$this->output->set_content_type ('application/json');

		// ---------------------
		// Grab the queue id
		// ---------------------
		$qid = $this->input->post('qid');

		// --------------------------------
		// Check qid is numeric
		// --------------------------------
		if(!is_numeric(intval($qid)) || $qid === 0){
			$this->output->set_output(json_encode(array(
					'status' => 400,
					'error_msg'  => "Bad Request. Missing queue id."
				)));
			return false;
		}
		
		/*
		// Testing response
		return $this->output->set_output(json_encode(array(
				'status' => '200',								
				//'error_msg' => 'Some decent error ocurred',
				'msg'    	=> 'Success',
				'db'    	=> 'ok',
				'dbname' 	=> 'bitlert_wp',
				'db_user' 	=> 'ok',
				'db_user_priv' => 'ok',
				'ftp_user' 	=> 'ok',				
				'package' 	=> 'ok',
				'install' 	=> 'ok',
				'ptime' 	=> 20
			)));
		exit;		
		*/

		// -----------------------------------------------------------------
		// Left join for queue and credentials
		// Important! This is a left join as we require the full set even 
		// if the table to the right does not exist
		// -----------------------------------------------------------------
		$query = $this->db->select('*')
					->from('queue')
					->join('queue_creds', 'queue.id=queue_creds.q_id', 'left')
					->join('queue_plugins', 'queue.id=queue_plugins.qid', 'left')			
					->where('queue.id', $qid)
					->limit(1)
					->get();

		// Return the row item
		$oQueue = $query->row();
		// ------------------------------------------------
		// Check if queue item not found
		// As any Qid can be passed in the POST this is
		// a very probable outcome
		// ------------------------------------------------
		if(!$oQueue){			
			$this->output->set_output(json_encode(array(
					'status' => 404,
					'error_msg'  => "Queue id not found"
				)));
			Logger::log('queue', 'Queue item ID: ' . $qid . ' not found');
			return false;
		}
		Logger::log('queue','-');
		Logger::log('queue', 'Processing started for domain: ' . $oQueue->domain);
		// --------------------------------------------------------
		// Convert the queue result object from the db to an 
		// associative array
		// --------------------------------------------------------
		$queue = get_object_vars($oQueue);

		try{
			// -----------------------------------------------
			// Try to fech the queue creds.
			// If they do not exist create a new record
			// -----------------------------------------------
			if(!$this->queue_creds->getById($qid)){
				$this->queue_creds->create(array('q_id' => $qid));
			}
			// -----------------------------------------
			// Clean the domain name
			// -----------------------------------------
			$clean_domain = str_replace(array('http://', 'https://', 'www.'), "", $queue['domain']);
			// ---------------------------------------------------
			// Prefix Treatment
			// ------------------------------------------------------------
			// Set the queue prefix if not set or if it's set
			// and longer than the spec
			// ------------------------------------------------------------
			if(empty($queue['prefix'])){
				$queue['prefix'] = substr($queue['cpanel_user'], 0, 8) . '_';			
			}

			// ----------------------------------------------------------------
			// If the prefix does not have a trailing underscore, add one
			// ----------------------------------------------------------------
			if(substr($queue['prefix'], -1) != "_"){
				$queue['prefix'] .= "_";
			}
			// ---------------------------------------------------
			// Save the queue prefix
			// Just in case we treat it somehow
			// ---------------------------------------------------
			$this->queue_creds->updatePrefix($qid, $queue['prefix']);

			// ---------------------------------------------------
			// End prefix treatment
			// ---------------------------------------------------

			// ----------------------------------------------------
			// Build the cPanel handler
			// ----------------------------------------------------
			$xmlapi = Cpanel_PublicAPI::factory('WHM', array(
								'host' 	   =>   $queue['cpanel_url'],
								'user' 	   =>	$queue['cpanel_user'],
								'password' =>   $queue['cpanel_psw']
							));
			$xmlapi->setPort('2083');

			$mysql = new MySQLHandler( $xmlapi, $queue['cpanel_user']);
			// --------------------------------------------------------
			// Check if the db name already exists
			// If we have a db name created, use that one
			// Otherwise create a new one by using the cpanel prefix
			// ---------------------------------------------------------
			if(empty($queue['dbname']))
			 	$queue['dbname'] = $queue['prefix'] . random_alpha(5);

			// ---------------------------
			// Create the Mysql database
			// ---------------------------
			$response = $mysql->createDatabase($queue['dbname'], true);
			// ----------------------------------------------------------------------------------------
			// Reset user privileges for newely created database
			// ----------------------------------------------------------------------------------------
			$queue['db_user_has_priv'] = 0;

			if(!$response){				
				throw new QueueException ('Failed to created the database');
			}
			// --------------------------------------------
			// Update the Queue creds model with dbname
			// --------------------------------------------
			$this->queue_creds->insert_dbname($qid, $queue['dbname']);
			$json_response['dbname'] = $queue['dbname'];
			// -----------------------------------------
			// Log positive outcome for db creation
			// -----------------------------------------
			Logger::log('queue', "Database {$queue['dbname']} created succesfully");
			$json_response['db'] = 'ok';

			// -------------------------------------------------------------------
			// Check if we have a user and password for the database
			// - If both are empty create a new db user
			// -------------------------------------------------------------------
			if (empty($queue['db_user']) && empty($queue['db_psw'])){
				// -------------------------------
				// Create a new database user
				// -------------------------------
				$queue['db_user'] = $queue['prefix'] . random_alpha(4);
				$queue['db_psw'] = generate_random_password (array(
										'length' => 14,
										'alpha' => 'on',
										'alpha_upper' => 'on',
										'numeric' => 'on',
										'special' => 'on'
									));

				$mysql_response = $mysql->createUser($queue['db_user'], $queue['db_psw']);

				if(!$mysql_response){					
					throw new QueueException ('Failed to create the db user');
				}

				// -------------------------------------------------
				// Update the Queue creds model with Db User data
				// -------------------------------------------------
				$this->queue_creds->insert_db_user($qid, $queue['db_user'], $queue['db_psw']);

				Logger::log('queue', 'Db user created succesfully');				

				// Reset privilege for new user
				$queue['db_user_has_priv'] = 0;
			}
			else{
				Logger::log('queue', 'Db user already created');
			}			

			$json_response['db_user'] = 'ok';

			// ----------------------------------------------------------
			// Generate privileges for the db user
			// ----------------------------------------------------------
			if ($queue['db_user_has_priv'] == 0){
				if(!$mysql->setUserPrivileges($queue['dbname'], $queue['db_user'], array('ALL PRIVILEGES'))){					
					throw new Exception('queue', 'Unable to set privileges for user ' . $queue['db_user']);
				}
			}
			// -----------------------------------------------------
			// Update the db user Privileges status in the system
			// -----------------------------------------------------
			$queue['db_user_has_priv'] = 1;
			Logger::log('queue', 'Privileges assigned for user ' . $queue['db_user']);
			$this->queue_creds->setUserPrivilege($qid, 1);			
			$json_response['db_user_priv'] = 'ok';

			// -------------------------------------
			//       *** FTP Management  ***
			// -------------------------------------
			$ftp = new FtpHandler($clean_domain ,$xmlapi, $queue['cpanel_user']);
			// -------------------------------------------
			// Create the FTP user if none in the system
			// -------------------------------------------
			if(empty($queue['ftp_user']) && empty($queue['ftp_psw'])){
				// ----------------------------------------------------------------------------------------
				// Create the ftp user
				// ----------------------------------------------------------------------------------------
				$response = $ftp->createFtpUser($queue['prefix']);

				if($response[0]->result == 0){					
					throw new FTPHandlerException("Ftp user creation failed");
				}

				$ftp_creds = $ftp->getFtpCredentials();	

				$queue['ftp_user'] = $ftp_creds['user'];
				$queue['ftp_psw'] = $ftp_creds['psw'];
				// ----------------------------------------------------------------------------------------
				// Add the Ftp credentials to the db
				// ----------------------------------------------------------------------------------------
				$this->queue_creds->insertFtpCredentials($qid, $queue['ftp_user'], $queue['ftp_psw']);
			}
			else{
				// Set the FTP credentials for this object
				$ftp->setCredentials($queue['ftp_user'],$queue['ftp_psw']);				
			}

			// ----------------------------------------------------------------------------------------
			// Positive json response for ftp_user
			// ----------------------------------------------------------------------------------------
			$json_response['ftp_user'] = 'ok';

			// ----------------------------------------------------------------------------------------
			// Set the root domain of this cpanel connection for the FTP connection
			// The true parameter sets the domain name in the FTP object
			// ----------------------------------------------------------------------------------------
			if(!$queue['is_root_domain'])
				$ftp->getRootDomain(true);

			/*==========================================
			=            Package Management            =
			==========================================*/
			
			$pm = new PackageManager($queue['package']);

			if(!$pm->upgradeWP()){
				Logger::log("queue", "Could not upgrade the WP in the package");
			}
			else{
				Logger::log("queue", "Wordpress version updated in package");
			}				

			// ----------------------------------------------------------------------------------------
			// Re-write the WP-Config Salts
			// ----------------------------------------------------------------------------------------
			if($pm->updateWPConfigSalts($queue['package'])){
				Logger::log("queue", "Wp-config Salts updated!");
			}
			else{
				Logger::log("queue", "Could not update the Wp Salts");
			}

			// ------------------------------------------
			// PATH to the "package to be uploaded"
			// ------------------------------------------
			$new_package_path = PACKAGE_ZIP_UPLOAD_PATH . DIRECTORY_SEPARATOR . $queue['package'];

			// ----------------------------------------
			// Upload the zip package to the server
			// ----------------------------------------
			if(!$this->avoid_package_upload_test){
				$ftp->uploadFile($new_package_path, REMOTE_DUPLICATOR_ZIP_FILENAME, 0755);
			}

			Logger::log("queue", "Package {$queue['package']} uploaded to the server as: " . REMOTE_DUPLICATOR_ZIP_FILENAME);
			// Package status ok
			$json_response['package'] = 'ok';
			
			/*=====  End of Package Management  ======*/	


			// ----------------------------------------------------
			// Create a Installer file from Installer Template
			// ----------------------------------------------------
			$installer = new InstallerHandler($queue);
			// Create the installer
			$installer_script = $installer->createInstallerFile();
			// ---------------------------------------------
			// Upload the installer script to the server
			// ---------------------------------------------
			$ftp->uploadFile($installer_script, INSTALLER_SCRIPT_NAME, 0644);

			// -----------------------------
			// Call the Installer via http
			// -----------------------------
			$install_response = $installer->runInstall(INSTALLER_SCRIPT_NAME);

			if(is_object($install_response) && intval($install_response->status) == 200){
				Logger::log("queue", "Script Installation succesfull");				
			}
			else{
				throw new InstallerHandlerException("Run install failed! Error: " . is_object($install_response)? $install_response->error_msg : "");
			}

			$json_response['install'] = 'ok';			
			// ------------------------
			// Do some file cleaning
			// -------------------------
			// Remove the installer file
			$ftp->removeFile(INSTALLER_SCRIPT_NAME);

			// Remove database.sql file
			$ftp->removeFile('database.sql');

			// ABove was the last $ftp
			$ftp->close();

			// ----------------------------------------------------------------------------------------
			// Remove the ftp user account
			// ----------------------------------------------------------------------------------------
			if($ftp->deleteFTPUser($queue['ftp_user'], $clean_domain)){
				$json_response['remove_ftp'] = "ok";
			}
			// ----------------------------------------------------------------------------------------
			// Remove the FTP credentials from the queue-creds
			// ----------------------------------------------------------------------------------------
			$this->db->where('q_id', $qid)
				->update('queue_creds', [
					'ftp_user' => '', 
					'ftp_psw' => ''		
				]);

			// --------------------------------------
			// Calculate processing elapsed time
			// --------------------------------------
			$total_time = (time() - $start_time);

			// Set total processing time in response
			$json_response['ptime'] = $total_time;

			Logger::log("queue", "Deployment completed succesfully!");
			Logger::log("queue", "Time elapsed: " . $total_time . " seconds");

			// --------------------------------------------
			// Set some complete vars for the queue
			// --------------------------------------------
			$this->db->where('id', $qid)
				 ->update('queue', [
				 		'date_run' => date("Y-m-d H:i:s"),
				 		'process_time' => $total_time,
				 		'complete' => 1
				 	]);			

			// ----------------------------
			// Set the final 200 status
			// ----------------------------
			$json_response['status'] = '200';

			// ----------------------------------
			// Send the final json output
			// ----------------------------------
			return $this->output->set_output(json_encode($json_response));
		}

		catch(Exception $e){
			// ---------------------------------------------------
			// Log the error
			// ---------------------------------------------------
			Logger::log('queue', $e->getMessage());
			$json_response['status'] = 500;
			$json_response['error'] = 'Exception raised';
			$json_response['error_msg'] = $e->getMessage();
			return $this->output->set_output(json_encode($json_response));
		}
	}


	private function _pre($var){
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}

class QueueException extends Exception{}
<?php

class Migrate extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');

                $current = $this->migration->current();

                // Check for Migration error
                if ($current === FALSE)
                {
                    show_error($this->migration->error_string());
                }
                else if($current === TRUE){
                	echo "No new migrations found. System ok.";
                }
                else{
                	echo "Migration run! New migration version is $current";
                }

        }

}
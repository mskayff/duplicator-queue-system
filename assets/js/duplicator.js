var Duplicator = function() {

	var queue = [];

	var is_stopped = false;

	var terminal = {

		 write : function(msg,type,isNewLine){
	
			var className = "", 
				text = "";

			switch(type){
				case  1  :  className = "green-text" ; 
						break;
				case 0  :  className = "red-text" ; 
						break;
				default :  className = "normal-text";
			}

			text = "<span class="+ className +">" + msg + "</span>";
			text += isNewLine? "<br>" : "";

			$('#terminal-panel').append(text);
		},

		lb : function (){
			$('#terminal-panel').append("<br>");
		},

		reset : function(){
			$('#terminal-panel').html("");
		},

		toggleAnimation : function(toggle){			
			if(toggle)
				$('#terminal-loader').show();			
			else
				$('#terminal-loader').hide();			
		}
	}

	
	var addEvents = function(){

		/**
		 * Show package modal
		 */
		$(".btn-remove-package").click(function(e){
			e.preventDefault();

			var qid = $(this).data('qid');
			// Set the qid data attribute in the delete button
			$('#btn-delete-package').data('qid',qid);
			$('#delete-package-modal').modal();
		});


		/**
		 * Delete one package event
		 */
		$('#btn-delete-package').click(function(e){
			// Grab the quid data attribute
			var qid = $(this).data('qid');

			$.ajax({

				url : 'duplicator/ajx_delete',
				method : 'post',
				data : { 'qid' : qid},
				dataType : 'json',
				beforeSend : function(xhr){
					// Hide the modal before send
					$('#delete-package-modal').modal('hide');
				},
				success : function(data){
					if(data.status == '200'){
						fadeItemOut(qid);
					}
					else{
						alert(data.msg);
					}				
				}			
			});
		});


		/**
		 * Show process queue modal
		 */
		$('#btn-process-queue').click(function(e){			
			$.ajax({
				url : 'queue/ajx_queue_details',
				method : 'GET',
				dataType : 'json',
				success : function (data){
					$('#confirm-package-count').html(data.count);
					// Assign the id items to queue_ids
					queue = data.items;
					$('#process-queue-modal').modal('show');
				},
				error : function(xhr){
					alert("An error occured. Check the Network output.");
				}
			});
		});


		/**
		 *
		 * Process queue button confirmation
		 */
		$('#btn-confirm-process-queue').click(function(e){
			$('#process-queue-modal').modal('hide');
			terminal.reset();						
			terminal.write("<b>Start queue processing...</b>", null, 1);
			processQueueItem(0);
		});

		/**
		 * Clean the terminal Window
		 *
		 */
		$('#btn-reset-terminal').click(function(e){
			terminal.reset();
		});

		$('#btn-stop-process').click(function(e){
			terminal.write ("Stopping queue. Process will stop on next iteration", 1,1);
			is_stopped = true;
		});

	}


	/**
	 * Process an individual item in the queue
	 */
	var processQueueItem = function(index){

		is_stopped = false;

		if(queue[index] != null){

			var queueItem = queue[index];

			$.ajax({
				url : 'queue/process_item',
				method : 'POST',
				data : { qid : queueItem.id},
				dataType : 'json',
				// Set a timeout of 10 mins.
				timeout : 600000,
				beforeSend : function(){
					terminal.toggleAnimation(true);
					terminal.write("Processing " + queueItem.site_url + "...", null,1);
				},
				success : function (data){
					// Succesfull processing
					if (data.status == '200'){
						swapQueueItemState (queueItem.id, 'success');
					}
					else{
						swapQueueItemState(queueItem.id, 'fail');
					}				
				},
				complete : function(xhr){
					console.log (xhr);						
					if(handleOutcomeResponse(xhr)){
						terminal.write ("Go to <a href='" + queueItem.site_url +  "' target='_blank'>Homepage</a> Go to <a href='" + queueItem.site_url + "/wp-admin/" +  "' target='_blank'>Wp-admin</a>", 1,1);
						terminal.lb();
					}
					
					// Check if the queue has been stopped first
					if(is_stopped){
						terminal.toggleAnimation(false);
						terminal.write("Queue Stopped!",0,1);
					}	
					else{
						processQueueStep (index);
					}						
				},
				error : function (xhr){
					console.log (xhr);
					swapQueueItemState(queueItem.id, 'fail');
					terminal.toggleAnimation(false);
				}
			});
		}
		else{
			terminal.write("Undefined element " + index + ".", 0, 1);
		}
	}


	var processQueueStep = function(index){

		// ---------------------------------------------------
		// Increment the index
		// ---------------------------------------------------
		index++;
		// ---------------------------------------------------
		// Check if there's a new element in the queue
		// ---------------------------------------------------
		if (queue[index] != null){
			terminal.toggleAnimation(true);
			// --------------------------	
			// Run the next iteration	
			// --------------------------
			processQueueItem (index)
		}
		else{
			// Do something with the queue finishing
			//alert ("Queue processing finished!");
			terminal.toggleAnimation(false);
			terminal.write("Queue has finished processing",null,1);
		}
	}


	var handleOutcomeResponse = function(xhr){
		// --------------------------------------------------------------------
		// We first check if the responseJSON is undefined
		// In this cases the reason would be a flaw in the code
		// List some sort of unhadled Exception/ Parse error / Fatal Error
		// ---------------------------------------------------------------------
		if(typeof xhr.responseJSON == 'undefined'){
			console.log(xhr.status);
			terminal.write ("An error ocurred. Contact support.",0,1);
			return false;
		}

		// ------------------------------------------------------------
		// Grab the json response
		// This object holds the whole json response representation
		// --------------------------------------------------------------
		var response = xhr.responseJSON;

		if(response.status != '200' && (response.error != null || response.error_msg != "")){

			// This should be rare. We always want some error response back.
			if(response.error_msg == "")
				response.error_msg = "Unidentified error ocurred";
			
			terminal.write(response.error_msg, 0,1)			

			return false;	
		}

		// responsebase outcome message
		if(response.db == 'ok'){
			terminal.write("Database created: " + response.dbname, 1,1);
		}				
		else{
			terminal.write('Database not created', 0,1);
		}		

		// Db user outcome
		if(response.db_user == 'ok'){
			terminal.write("DB user created",1,1);
		}				
		else{
			terminal.write("Db user failed!",0,1);
		}	

		// Db privileges outcome
		if(response.db_user_priv == 'ok'){
			terminal.write("Db privileges set!",1,1);
		}				
		else{
			terminal.write("Db privileges not set", 0,1);
		}	

		// Ftp user outcome
		if(response.ftp_user == 'ok'){
			terminal.write("Ftp user created!", 1,1);
		}				
		else{
			terminal.write("Ftp not created",0,1);
		}	

		// Package upload outcome
		if(response.package == 'ok'){
			terminal.write("Package uploaded succesfully!",1,1);
		}				
		else{
			terminal.write("Package upload failed", 0,1);
		}	

		// Package install outcome
		if(response.install == 'ok'){
			terminal.write("Package installed succesfully!",1,1);
		}				
		else{
			terminal.write("Package installation failed", 0,1);
		}	

		if(response.remove_ftp && response.remove_ftp == "ok"){
			terminal.write("FTP accounts removed ok",1,1);
		}
		else{
			terminal.write("FTP accounts could not be removed",0,1);
		}

		if(response.ptime != null){
			terminal.write ("Total processing time: " + response.ptime + " seconds",null,1);
		}		

		return true;
	}


	var fadeItemOut = function (qid){
		$('#queue-item-' + qid).fadeOut(700);
	}

	var swapQueueItemState= function(qid, state){
		$('#queue-item-' + qid).hide();
		$('#queue-item-' + qid).removeClass("queue-item-idle queue-item-fail queue-item-success");
		$('#queue-item-' + qid).addClass("queue-item-" + state);
		$('#queue-item-' + qid).fadeIn(500);
	}


	return {

		init : function (){
			addEvents();
		}		
	}
}();


$(document).ready(function(e){	
	// ----------------------------------------------------------------------------------------
	// Init the duplicator
	// ----------------------------------------------------------------------------------------
	Duplicator.init();	
});


